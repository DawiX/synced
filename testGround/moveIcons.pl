#!/usr/bin/perl
use strict;
use warnings;
use File::Basename;
use File::Copy;


print "Define the file containing list of destination directories \n";
my $list = <STDIN>;

print "Define the trunk root directory\n";
my $root = <STDIN>;
chomp($root);
#print "$root";

open INPUTFILE, $list; 
my @inputfile = <INPUTFILE>;
close INPUTFILE;

foreach my $line (@inputfile) {
	chomp($line);
	my $dest = $root . $line;
	my $file = basename($line);
	copy($file, $dest); 
#	print "$dest";	
#	print "$file";
#	print "$line";
}
	

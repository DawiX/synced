<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>

        <title>Using File Import</title><link rel="StyleSheet" href="../../ijc.css" type="text/css"></head>
    <body>
        <p style='text-align:center'><a href='../ijcTOC.html'>Table of Contents</a></p>
        <h2>Using File Import</h2>

        <ul>
            <li><a href="#supported_formats">Supported File Formats</a></li>
            <li>
                <a href="#wizard_usage">Using File Import Wizard</a>
                <ol>
                    <li><a href="#step1">Project and Connection Selection</a></li>
                    <li><a href="#step2">File and Table Details</a></li>
                    <li><a href="#step3">Field Details</a></li>
                </ol>
            </li>
            <li><a href="#secondary_structures">Importing Secondary Structures</a></li>
        </ul>

        <p>The File Import wizard is a simple and fast way for you to import data into IJC.
            You can import a file into a schema in an already opened project.
            You must have sufficient access rights for the schema to do this, but you
            should always have this in a local derby database.
        </p>

        <h3 id="supported_formats">Supported file formats</h3>
        <p>
            IJC supports most of the commonly used file formats:
        </p>

        <table cellpadding="3" border="1">
            <tr>
                <th>Format</th>
                <th>File extension</th>
                <th>Comments</th>
            </tr>
            <tr>
                <th>MRV</th>
                <td>*.mrv</td>
                <td>ChemAxon's XML based Marvin file format that contains structures
                    plus data fields for those structures. This format is recommended when
                    transferring data from other ChemAxon software as it support all the Marvin
                    structure features.
                </td>
            </tr>
            <tr>
                <th>SDF</th>
                <td>*.sdf</td>
                <td>MDL's SD file format that contains structures plus data fields
                    for those structures. This format is recommended for transfer from
                    most other non-ChemAxon software as it supports the most
                    structural features and is supported by a wide range
                    of other software.
                </td>
            </tr>
            
            <tr>
                <th>JC4XL XLS</th>
                <td>*.XLS</td>
                <td> It is now possible to import a JChem for Excel XLS file directly into Instant JChem !
                     This marvellous new feature allows for perfect inter-operability between ChemAxon desktop products.
                     From the import dialog you can specify your XLS and it will be imported and mapped directly to 
                     an IJC entity. If any of the "Chemistry Object", SMILES or IUPAC exists in the source XLS, 
                     then a Structure entity is created with that source used to create the internal structure column.
                    
                </td>
            </tr>
            
            <tr>
                <th>PDF</th>
                <td>*.pdf</td>
                <td>PDF's that contain chemical trival names and formal nomenclatures can now be parsed and imported into chemical structure tables. 
                    Every instance of a recognised chemical name in a PDF document will be converted to a single record in the table. 
                    Please see the supporting script named <a href="http://www.chemaxon.com/instantjchem/ijc_latest/docs/developer/scripts/pdf_trawler.html">pdf_trawler</a> which shows
                    this new feature in action processing multiple input files.
                    
                </td>
            </tr>
            <tr>
                <th>RDF</th>
                <td>*.rdf</td>
                <td>MDL's RD file format that contains structures plus data fields
                    for those structures. This format is like an extended, but less well
                    defined, version of the SD file format and is commonly used for
                    containing reactions and/or relational (hierarchical) data.
                    Standard import only imports this data as flat (non-relational)
                    data, but the <a href="rdf_import.html">RDF import</a> provides
                    a way to import relational data.
                </td>
            </tr>
            <tr>
                <th>Smiles, smarts</th>
                <td>*.smi, *.smiles, *.smarts</td>
                <td>Daylight's smiles and smarts file formats that just contains
                    structures as a text string, one structure on each line of the file.
                    This format is very lightweight as it does not
                    contain 2D or 3D coordinates and so is suitable for transferring
                    very large numbers of structures in small files. As coordinates are
                    not present they will be generated on-the-fly when being displayed
                    so performance will be slightly slower.
                    The smiles format allows additional data to be provided on each
                    line after the structure definition. IJC will generally support
                    these extra fields, but it does depend on how they are supplied.
                    Importing as TAB or CSV format files is usually better in this
                    case.
                </td>
            </tr>
            <tr>
                <th>Cxsmiles, cxsmarts</th>
                <td>*.cxsmiles, *.cxsmarts</td>
                <td>ChemAxon's extended smiles and smarts file formats that extends
                    the Daylight formats to include Marvin features that are not supported
                    by the Daylight format. This extra information is supplied in a way
                    that is compatible with the Daylight syntax.
                </td>
            </tr>
            <tr>
                <th>InChi</th>
                <td>*.inchi</td>
                <td>IUPAC's InChi file format which contains only structure definitions
                    (no data fields).
                </td>
            </tr>
            <tr>
                <th>Text files</th>
                <td>*.csv, *.tab, *.txt</td>
                <td>Delineated text file formats usually containing text and numeric
                    data, but possibly also structures in a format that is compatible with the
                    "single line" nature of this format (this effectively means it is limited to
                    smiles and the various variants of smiles). The fields in the data
                    file are separated by a constant separator (TAB, comma, space,
                    colon and semi-colon are supported). Field names of sometimes included
                    as the first line of the file, and there is an option to use this
                    first line for field names should this be the case.
                </td>
            </tr>
            <tr>
                <th>IUPAC name</th>
                <td>*.name</td>
                <td>IUPAC names can be imported and converted to structures using
                    the Name2Structure calculator plugin.
                    This option is only available if you have a license for this
                    calculator plugin.
                    The required format is a text file with each structure name
                    on a separate line.
                </td>
            </tr>
            <tr>
                <th>Markush DARC</th>
                <td>*.vmn</td>
                <td>Thomson Reuters Markush DARC (*.vmn) files contain Markush structures
                    extracted from patents. This is the &quot;Derwent patent database&quot;,
                    now owned and distributed by Thomson Reuters. These files are particularly useful
                    when imported into a JChem table of type Markush libraries as you can search
                    the table of Markush structures using substructure or exact structure searches.
                </td>
            </tr>
        </table>

        <h3 id="wizard_usage">To use the File Import wizard:</h3>

        <p>You can open the File Import wizard using any of the following methods:</p>

        <ul>
            <li>Start by choosing Import File into IJC under the Quick Start heading from
                the Welcome Screen.</li>
            <li>Right-click a <a href='javascript:void(0)' onclick="window.open('../keywords/schema.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Schema</a>
                node (<img src="../../iconImages/Connection16.gif"> or <img src="../../iconImages/ConnectionRemote.gif">)
                in the Projects window and choose File import.... This allows you to import
                your file into a <strong>new</strong> database table directly within the selected schema.</li>

            <li>Right-click a
                <a href='javascript:void(0)' onclick="window.open('../keywords/data_tree.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Data Tree</a>
                node (
                <img src="../../icons/datatree.png">&nbsp;
                <img src="../../icons/datatree-simple-std.png">&nbsp;
                <img src="../../icons/datatree-simple-jcb.png">
                ) in
                the Projects window and choose File import.... Alternatively, choose File
                -&gt; File Import... from the main menu.</li>
            <li>Choose File import... from the File menu or from the main toolbar when you
                have a view (e.g. the
                <a href='javascript:void(0)' onclick="window.open('../keywords/grid_view.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Grid View</a>
                ) selected. The file will be imported into a <strong>new</strong> database table directly
                within the schema in which you are currently working.
                .</li>
        </ul>

        <p>Import supports two distinct scenarios:
        <ol>
            <li>Import into a new database table (entity)</li>
            <li>Import into an existing database table, adding the data from the file
                to the data that already exists.</li>
        </ol>
        The distinction is important, and in IJC 2.4 we made a change to how the
        import action works to avoid the risk of importing data into an existing table
        by mistake. Normally import works by creating a new table and importing the data
        into it. The File menu items, the toolbar icons and the right click menu item
        on the schema node in the projects window all work in this way. If you
        want to import data into an existing table you must use the 'File import...'
        action in the right-click menu on the data tree in the projects window.

        Note the two screenshots below. In the left hand one we are importing a
        file into the localdb schema as a NEW table, whilst in the right hand one we
        are importing the file into the EXISTING Pubchem demo table.

    </p>

    <br>
    <table border="0" cellpadding="5">
        <tr>
            <td>
                <img alt="Import into new entity" src="../../screenShots/file-import-new.png">
            </td>
            <td>
                <img alt="Import into existing entity" src="../../screenShots/file-import-into.png">
            </td>
        </tr>
    </table>
    <br>
    <p><a name="step1"></a><strong>Step 1. Project and Connection Selection:</strong></p>
    <p>This step will only appear when you have started the import without a context
        being defined (e.g. from the Welcome Screen), or you do not have sufficient
        access rights to import into your currently selected schema. You may not yet have
        connected to any schemas so this screen will help you through the process of
        selecting and/or connecting to your database. When the connection or database
        table is specified by the way in which you open the import wizard, this step
        is automatically skipped.</p>

    <br>
    <img alt="Step 1" src="../../screenShots/fileImportScreen1.png">
    <br>


    <p><a name="step2"></a><strong>Step 2. File and Table Details:</strong></p>

    <p>Specify the details of the file and the database table by working from
        the top to the bottom of this screen.</p>

    <ol>
        <li>Specify the file by clicking the button to the right of the File to Import
            field.</li>
        <li>The type of file should be automatically detected and the appropriate parser
            selected in the next combo box. If this is not correctly specified then you will
            need to change the selection. You may also need to change some of the parser options
            which can be done using the '...' button next to the parser selector. These
            options differ for the different parser types. Amongst the most useful options are:
            <ul>
                <li>Option to convert 3D structures to 2D before import</li>
                <li>Option to set the chiral flag for the imported structures (for
                    structure formats using old-style MDL stereochemistry)</li>
                <li>Option to treat the first line of comma or tab separated format
                    files as the field names and not as data</li>
            </ul>

        </li>
        <li>The table type should be automatically detected, but if this is not correct you
            will need to specify it manually in the next combo box. You might also
            need to adjust the table options
            by clicking on the '...' button next to the table selector. For JChem
            tables there are several important options that can affect the import
            as well as your options for structure searching and many of these
            can only be specified when the table is created, not afterwards.
            <strong>Note: </strong>If importing into an existing
            database table these options are disabled as they have already been specified.</li>
        <li>Click Next. The database table you specified will be created (if using a new
            table).</li>
    </ol>

    <br>
    <img alt="Step 2" src="../../screenShots/fileImportScreen2.png">
    <br>

    <p>The wizard automatically reads the records from the file and shows you the data
        fields that have been found. There may be a slight delay as this happens,
        depending on the speed of your computer and the amount and type of data contained in
        the file. The fields will be re-read if you change the file, the processing
        options or the table type.</p>

    <p>As the file is read, the details for the new table will be updated accordingly.
        For example, if empty structures are found, the setting for the JChem table will
        be updated to allow empty structures, and if the file is found to contain only
        reactions then the table type will be changed to be specific for reactions.</p>

    <p>The 'Records Read' text field indicates the number of records that have been read
        so far, and the data fields found in those records are displayed. By default the
        file is read until no new records are found once 100 new records have been read.
        This process continues until no new fields are found. Whilst this usually gives a
        good indication of the contents of the file, it is possible that some new fields
        will be found later in the file, or that a reaction will be found as the last record
        of a file that otherwise contains only normal molecules. Use the 'Read more' button to read
        extra records if you think that this may be the case; you may need to read many,
        sometimes all, of the records. Set the Read More value to zero to read the whole
        file.</p>

    <p>Note that although the automatic detection of the file contents usually works well
        it cannot be expected to work for all files. You should check that the settings
        on this page and the next one for field details are what you expect.</p>



    <br>
    <p><a name="step3"></a><strong>Step 3. Field Details:</strong></p>

    <p>In this step you specify how the fields found in the file should be handled.
        This step is very powerful as it lets you specify the type of new fields
        being created, the mapping between fields in the file and those in the
        database, and whether some fields should be used as 'merge fields' (see
        <a href="merging_data.html">Merging Data</a> for details on how to do this).</p>

    <p>However, although it is powerful, in the usual case of importing a file into
        a new database table the settings are automatically detected and you typically
        only need to click Next to start the import.</p>

    <br>
    <img alt="Step 3" src="../../screenShots/fileImportScreen3.png">
    <br>
    <br>

    <p>This screen is divided into 3 regions:</p>

    <ol>
        <li>A list of fields from the file that is displayed in the top left.</li>
        <li>A list of fields from the database table that is displayed in the top right.</li>
        <li>Options for the selected database field that is displayed at the bottom.</li>
    </ol>

    <p>The key component is the one with the fields in the database table which
        is displayed in the top right corner. This lists the fields that are already
        present in the table (either were in the table before we started the import or
        were default fields created when the table was created). Below these are the new
        fields that will be added to the database as part of the import (indicated by
        the + symbol next to the field and the new field icon).</p>

    <p>By default, all fields found in the file are specified as needing to be added to
        the table as new fields. If some of these are not needed, select them from the
        list on the right and click the 'Remove' button. The field will then become enabled
        in the list on the left side, signifying that it can be added to the list of
        database fields to create by clicking the 'Add' button.</p>

    <p>The order of the added fields can be adjusted using the 'Move down' and 'Move up'
        buttons.</p>

    <p>If instead of wanting to add the field as a new field you may want to use an existing
        field in the table (this only applies to existing tables that have had extra fields
        added). You can specify this by selecting the file field and the database field and
        using the 'Map' button. Similarly you can use the 'Merge' button to specify that
        you want to merge the data using this field. Merging is described in more detail
        in <a href="merging_data.html">Merging Data</a>. Mapped and merged fields are
        indicated by the single and double-headed arrow symbols.</p>

    <p>When a field is selected from the database list, its options are displayed in the
        lower panel. For new fields, you can specify the options for the new field. For
        existing fields there may be the option to specify a default value to use if the
        field is not mapped or does not have a value in the file when mapped.</p>

    <p><strong><a name="step4"></a>Step 4. Monitor import:</strong></p>

    <p>The import starts. The whole file is read and the fields specified in the previous
        step will be imported, along with the structures. Any records in the file will be
        written to an errors file and you will be notified of this. If a new database table was
        created then a new Data Tree and Grid View will be created so that you can immediately
        see the results of your import. You can specify that
        this should be displayed once the import finishes by clicking the Display Data When
        Finished checkbox.</p>

    <br>
    <img alt="Step 4" src="../../screenShots/fileImportScreen4.png">
    <br>
    <br>

    <p>The import can be terminated at its current position at any stage by clicking
        the Cancel button.</p>
    <h3 id="secondary_structures">Importing file containing secondary structures</h3>
    <p>Although only one structure field is supported in standard JChem table, secondary structure (and even more) can be present in the text field.
    There are some limitations in usage and deviations from standard procedure you need to follow when importing such a file.
    </p>
    <ul>
        <li>
            Text-fields have the default length set to 1000 and it is not enough for the structures.
            Extend the length of the secondary structure field, 10000 should be sufficient for most cases.
            The usage of CLOB type instead of VARCHAR2 is also possible.
        </li>
        <li>
            Set Mime-Type of the field to text/structure as seen in the figure. This is particularly
            very important for compatibility of exported RD files containing secondary structures.
            It also determine the chose of the renderer in newly created views
            (or widgets like sheet or table) in the way that structure renderer is used.
            <img alt="Extra attributes" src="../../screenShots/ImportSnStrucExtraAttr.png"><br><br>
        </li>
        <li>
            If you use Sheet or Table widget, change the Cell Renderer to Structure Renderer.
            Molecule pane can be bound to that field without any additional work.
            Please refer to <a href="../managing_data/renderers.html">Renderers page</a> for more details.
            <img alt="Set renderer" src="../../screenShots/ImportSnStrucRenderer.png"><br><br>
        </li>
    </ul>

            <br/>
            <hr/>
            <p style="font-size:xx-small;text-align:center">Copyright &#169; 1998-2013
            <a href='http://www.chemaxon.com' target='_top'>ChemAxon Ltd.</a>
            All rights reserved.</p>
            <br>

</body>
</html>

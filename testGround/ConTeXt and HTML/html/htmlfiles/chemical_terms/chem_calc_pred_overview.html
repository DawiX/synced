<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    
    <title>About Chemical Calculations and Predictions</title>
    <link rel="StyleSheet" href="../../ijc.css" type="text/css">
</head>
<body> 
<p style='text-align:center'><a href='../ijcTOC.html'>Table of Contents</a></p>
<h2>About Chemical Calculations and Predictions</h2> 
<h3>What are Chemical Calculations and Predictions?</h3>

<p>Calculations and predictions generate values for properties of a particular 
    chemical structure. A calculation is something that generates a value 
    for that structure (e.g. number or atoms, molecular weight) whereas a 
    prediction generates an estimated value for a property that cannot be
    precisely determined, except by experimental methods (e.g. logP, pKa, 
    solubility), though this distinction is often somewhat blurred. There are 
    usually multiple ways to generate a prediction (e.g. different computer 
    algorithms and/or different parameters) and different ways will generate 
different values.</p>

<p>When using predictions it is desirable to test the prediction against actual
    (experimentally determined) values to establish how "good" the 
    prediction is. Typically no one prediction method is "right" and 
    all others "wrong", but some may be generally better than others, 
    and it is important to select the best algorithm and/or parameters for your 
    particular need. Also bear in mind that the "best" approach may well 
    be different for different chemical series, and that some approaches can be 
    improved by training using particular verified (experimentally determined) 
values.</p>

<p>Whilst the distinction between the accuracy of calculations and predictions
    is important when interpreting the results, from Instant JChem's point 
    of view both are just "properties" that can be calculated from a 
    chemical structure. For this reason the term "property" will be 
    used unless there is a need to distinguish between a calculation and a 
prediction.</p>

<h3>ChemAxon's Chemical Terms and Calculator Plugins</h3>

<p>ChemAxon's approach to calculations and predictions is to make a wide 
    range of properties available to you, and to provide an extensible 
    architecture that allows new calculations to be added. This architecture
    is "calculator plugins", which allows chemical properties to be 
"plugged in" in an extensible manner.</p>

<p>Each calculator plugin provides one or more chemical property. A particular
    calculator plugin typically contains a related set of properties (e.g.
    the Protonation plugin includes properties for pKa, Major Microspecies
    and Isoelectric Point). Most plugins require additional licenses to run,
    though some basic properties are available without licenses. If you need 
    licenses email sales at chemaxon dot com
</p>

<p>A list of calculator plugins that are currently available can be seen
    <a href="https://www.chemaxon.com/marvin/help/calculations/calculator-plugins.html">here</a>
</p>

<p>The calculator plugin architecture allows new calculations or predictions
    to be added to JChem. This topic is outside the scope of Instant JChem. See
    <a href="https://www.chemaxon.com/marvin/doc/dev/plugins.html#custom">here</a>
    
for a guide.</p>

<p>Chemical properties are available in Instant JChem in 2 different ways:</p>

<ol>
    <li>When editing an individual structure using MarvinSketch. These are 
        available from the Marvin Tool menu. Please consult
        <a href="https://www.chemaxon.com/marvin/help/calculations/calculator-plugins.html">Marvin documentation</a>
       <br> 
        
    for details on using the calculator plugins in Marvin.</li>
    
    <li>Accessing chemical properties through Chemical Terms expressions that 
        allow simple or complex chemical property expressions to be defined. 
        This way is probably much more useful in Instant JChem, as you can
        either add chemical properties to your database as a new field or 
    use an additional filter expression when running queries.</li>
</ol>

<h3>Chemical Terms Expressions</h3>

<h4>Simple Expressions</h4>

<p>A Chemical Terms expression is much like a mathematical formula, in that 
    it contains one or more terms that are evaluated to generate a result. 
    This is best illustrated by means of an example. One of the simplest forms
    of Chemical Terms expressions is:
    
    <br><br><code>atomCount()</code><br><br>
    
    which as you might expect returns the number of atoms in the current molecule.
    Similarly the expression
    
    <br><br><code>logP()</code><br><br>
    
    specifies the predicted logP of the structure, and the expression
    
    <br><br><code>PSA()</code><br><br>
    
specifies the toplogical polar surface area of the structure.</p>

<p>Some expressions can take parameters, such as 
    
    <br><br><code>PSA('7.4')</code><br><br>
    
    which predicts the toplogical polar surface area at pH7.4. To find a full description 
    of the huge list of different chemical terms functions please consult the 
    <a href="https://www.chemaxon.com/marvin/help/chemicalterms/EvaluatorFunctions.html">Chemical Terms Reference Guide</a>.</p>

<h4>Combining Functions into Complex Expressions</h4>

<p>Multiple Chemical Terms functions can be combined into more complex expressions
    using arithmetical (+, -, *, /), logical (&amp;&amp;, ||, !) or comparison 
    (&lt;, &lt;=, &gt;, &gt;=, =) operators. Although conceptually simple, this provides 
    a very useful mechanism to build powerful Chemical Terms expressions. 
This is best illustrated by means of some examples:</p>       

<table border="1" cellpadding="10">
    <thead>
        <tr>
            <th>Expression</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>logd("7.4") - logD("3.8")</td>
            <td>logD at one pH 3.8 subtracted from logD at pH 7.4</td>
        </tr>
        <tr>
            <td>logP() &lt; 5</td>
            <td>logP must be less than 5</td>
        </tr>
        <tr>
            <td>(mass() &lt;= 500) &amp;&amp; <br>
                (logP() &lt;= 5) &amp;&amp; <br>
                (donorCount() &lt;= 5) &amp;&amp; <br> 
            (acceptorCount() &lt;= 10)</td>
            <td>Lipinski Rule of 5 filter. <br> 
                The &amp;&amp; symbol means AND, so this rule 
            passes when all of the terms evaluate to TRUE</td>
        </tr>
        <tr>
            <td>(mass() &lt;= 500) +<br> 
                (logP() &lt;= 5) +<br> 
                (donorCount() &lt;= 5) +<br> 
                (acceptorCount() &lt;= 10) +<br> 
                (rotatableBondCount() &lt;= 10) +<br> 
                (PSA() &lt;= 200) +<br> 
            (fusedAromaticRingCount() &lt;= 5) &gt;= 6</td>
            <td>A bioavailability filter.<br> 
                6 out of the 7 filters must pass for 
                the whole rule to evaluate to TRUE. In the context of the + 
                operator the value or the individual rules is 0 (fail) or 1 (pass), 
                and so this expression is just adding up the 1's to find out
                how many individual terms have passed, and then checked whether this
            total is greater than or equal to 6</td>
        </tr>
    </tbody>
</table>

<p>The important thing to emphasize here is that the comparison operators 
    (&lt;, &lt;=, &gt;, &gt;=, =) can be used to convert a text or numeric 
    value into a BOOLEAN (true or false) value, and that the logical operators 
    (&amp;&amp;, ||) are used for combine boolean operators into composite terms where 
all (or a defined combination) must match for the outcome to be true.</p>

<p>As you can see, the types of expressions you can build up can be very 
    powerful, and it is easy to build your own expressions or adapt existing 
    expressions to your needs. More complete documentation is available in the
    <a href="https://www.chemaxon.com/jchem/doc/user/ChemicalTerms.html">Chemical Terms Language Reference</a>
    <br>
    
    Please consult this if you want to use chemical terms expressions for
anything but the simplest of purposes.</p>

<h4>Complex Return Types</h4>

<p>The expressions we have discussed so far have simple return types. These
    are numbers, text or boolean. Whilst these are currently the most useful 
    types in Instant JChem, there are many other types of chemical terms 
    expressions that return complex types. A complex type is a value that is 
not a simple value. Examples are:</p>

<table border="1" cellpadding="10">
    <thead>
        <tr>
            <th>Expression</th>
            <th>Return type</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>apKa()</td>
            <td>Returns the acidic pKa values of all protonisable atoms.<br>
            The return type is an array, in descending order (strongest pKa first).</td>
        </tr>
        <tr>
            <td>majorMs("7.4")</td>
            <td>Returns a structure for each of the major microspecies at pH 7.4.<br>
            The return type is one or more Molecules</td>
        </tr>
        <tr>
            <td>tautomers()</td>
            <td>Returns a molecule for each of the tautomers of the structure.<br>
            The return type is one or more Molecules.</td>
        </tr>
    </tbody>
</table>

<p>Whilst these types of chemical terms expressions are not currently directly usable
    in Instant JChem (largely because there is no sensible way to display the results 
    within the restrictions of the 
   <a href='javascript:void(0)' onclick="window.open('../keywords/grid_view.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Grid View</a>
) they are worth mentioning for 2 reasons:</p>

<ol>
    <li>They can be "coerced" into simple values by use of basic functions
        (e.g. min(), max(), sum(), count()). For instance you could display the number 
    of tautomers using <code>count(tautomers())</code>.</li>
    <li>These Chemical Terms types will be increasingly useful with the Form
        view that was introduced in Instant JChem 2.0. For instance,
        you will be able to display all the tautomers for the current structure 
    as a component on the form.</li>
</ol>

<h3>Using Chemical Terms Expressions in Instant JChem</h3>

<h4>Adding a Chemical Terms Field</h4>

<p>The most direct way to use chemical terms expressions is by adding a 
    Chemical Terms field to a JChem 
    <a href='javascript:void(0)' onclick="window.open('../keywords/entity.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Entity</a>.
    This is just like adding a standard
    field, but instead of manually entering the values they are automatically 
    generated for each structure using the specified chemical terms expression. 
    For example, to add values for the predicted logP value you would add a 
    Chemical Terms field and specify the expression logP() for the chemical terms 
    expression. Once the field is added, all the logP values for the structures in 
    the table will be generated (this may take some time - minutes or even hours, 
    depending on how many structures the database table contains). Once complete, 
    you not only have logP values for all the structures, but they will also be 
    automatically updated if the structures are edited or any new structures added. 
    The values are therefore "live" and will remain accurate after you  
    make any changes. Also, because these values are automatically generated based 
on the structure, you cannot edit them directly.</p>

<p>There is a little bit of magic going on as calculations are performed.
    The current molecule is set as an "invisible" parameter to the calculation, 
    but whilst this is completely transparent to you as the user of a chemical terms 
    expression, it may be useful to point this out as you can use this molecule
    as part of the input parameters to the calculation. For example, you can use 
    the atom numbers of the input structure as part of the chemical terms expression.
    For a full description of this advanced functionality see the 
    <a href="https://www.chemaxon.com/jchem/doc/user/ChemicalTerms.html">Chemical Terms Language Reference</a>
   .<br> 
    
</p>

<p>Any Chemical Terms column can be used as part of a query or sort directive in 
    Instant JChem. For example, once you add a Chemical Terms field to your
    JChem table you can use those values as part of a query or use those values
to sort the data, just as if it were a normal field.</p>

<h4>Using a Chemical Terms Expression as an Additional Filter to a Query.</h4>

<p>Sometimes you may just have a particular question you want to ask of your 
    structures. You just want the see the structures that match your particular
    requirements, and don't want to go through the process of adding all the 
    Chemical Terms fields to generate the values that you need. This is where a
Chemical Terms filter becomes useful.</p>

<p>A Chemical Terms filter lets you apply the results of a Chemical Terms 
    expression to a query. This filter gets applied as the last step of running
    the query, removing structures from the results that don't pass the Chemical 
    Terms filter. The Chemical Terms filter gets applied to EVERY structure that 
    comes out of the execution of the query. This can be very useful, but it 
    also runs the risk of specifying a query that can take an extremely long
time to complete.</p>

<p>So a Chemical Terms filter can be very useful, but needs to be applied
    with some thought. If used as a way of refining a well defined query then 
    it should work very well, but if you specify a complex Chemical Terms filter 
    without a sensible query to pre-filter the results, your query will take 
    a long time to complete. For instance, if you don't apply any other query 
    criteria and try to search a database containing 1 million structures with 
    a complex filter, for example a Lipinski rule of 5, then you should go and 
    make yourself a coffee whilst the query executes (fortunately the query can 
be cancelled if you get fed up waiting once your coffee has gone cold!).</p>

<h4>Which is Better? Chemical Terms Field or Chemical Terms Filter?</h4>

<p>While this choice generally depends on your needs, a Chemical Terms field is usually 
    better because the values only need to be generated once. However, that doesn't
    mean that Chemical Terms filters don't have their place. If you do decide to use a 
    Chemical Terms filter, make sure you spend a moment considering whether you are 
    proceeding in an optimal manner. Chemical Terms filters can be a useful way of 
    refining queries, but if you specify a filter badly you can convert a fast query 
into something that takes several minutes (or hours!) to complete.</p>

<h3>Conclusion</h3>

<p>Chemical Terms provides a very powerful extension to Instant JChem. We hope this
    summary has whet your appetite to the incredible power of Chemical Terms. But 
    please consult the full documentation
    <a href="https://www.chemaxon.com/jchem/doc/user/ChemicalTerms.html">Chemical Terms Language Reference</a>
    
    if you think Chemical Terms will be useful to you. This overview 
just skims the surface of what Chemical Terms can do for you!</p>


        <br/>
        <hr/>
        <p style="font-size:xx-small;text-align:center">Copyright &#169; 1998-2013
        <a href='http://www.chemaxon.com' target='_top'>ChemAxon Ltd.</a>
        All rights reserved.</p>
        <br>

</body>
</html>

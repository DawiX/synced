<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>    
        <title>Markush Enumeration</title>
        <link rel="StyleSheet" href="../../ijc.css" type="text/css">
    </head>
    <body>
        <p style='text-align:center'><a href='../ijcTOC.html'>Table of Contents</a></p>
        <h2>Markush Enumeration</h2>

        <p>New to IJC 2.3 was the ability to handle databases of Markush structures.
            Associated with this is the ability to enumerate a Markush structure, and 
            to restrict the enumerated structures to those that match the current query.
        </p>

        <p>Markush structures are commonly used to describe combinatorial libraries or
            patent claims. It is assumed that the reader has basic knowledge of Markush 
            structures.
        </p>

        <h3>Background</h3>
        <p>The underlying Marvin and JChem tools provide support for handling Markush structures
            in the following ways:</p>
        <ol>
            <li>Marvin allows drawing and display of structures with Markush features,</li>
            <li>Marvin allows enumeration of a Markush structure to generate some or all of
                the discrete structures described by the Markush definition. This requires a
                Markush Enumeration license.
            </li>
            <li>JChem allows a table of Markush structures to be created and searches
                run against that table. This allows you, for instance, to perform a
                substructure search against a database of Markush structures to find
                all Markush definitions that includes the query structure as a substructure.
                This is very useful, for instance, when searching patent databases.
                Markush Search and Markush Enumeration licenses are required.
            </li>
            <li>JChem allows Markush Enumeration to be performed within the context of a
                query structure, so that the structures that are enumerated are restricted
                to those that match the query structure.
                Markush Search and Markush Enumeration licenses are required.
            </li>
        </ol>
        Instant JChem allows you to perform all of these operations.



        <h3>1. Drawing Markush structures in Marvin Sketch</h3>

        <p>Please consult the
            <a href="https://www.chemaxon.com/marvin/help/sketch/sketch-basic.html#markush-structures">Marvin Sketch documentation</a>.
        </p>

        <h3>2. Markush Enumeration in Marvin</h3>

        <p>Marvin Sketch provides a Markush Enumeration plugin. This is a feature
            of Marvin, not Instant JChem, but can be used in Instant JChem whenever
            Marvin Sketch is used. For more details please consult the
            <a href="https://www.chemaxon.com/marvin/help/calculations/markush.html">Marvin Markush Enumeration plugin documentation</a>.
        </p>

        <h3>3. Creating and searching Markush tables</h3>
        <p>Creating tables is described in the <a href="../editing_database/editing_entities.html">Editing Entities</a>
            help page.</p>


        <h3>4. Enumerating Markush structures</h3>
        <p>Instant JChem has special support for enumerating Markush structures.
            To do this you first need to create a JChem table containing the Markush
            structures.
        </p>

        <h4>Opening the Markush Enumeration dialog</h4>

        <p>Once you have a Markush structure table you can view the contents using the
            standard form or grid view. You can also run structure searches (most typically
            substructure searches) against this table to find only those Markush structures
            of interest. When you are viewing the contents of the Markush table in the grid
            or form view you can choose to enumerate any particular structure. Select the structure
            you want to enumerate and click on the 'Enumerate a Markush Structure' icon  
            (&nbsp;<img src="../../iconImages/MarkushEnum.png">&nbsp;)
            in the toolbar. The Markush Enumeration dialog will open.
            <br>
            <br>
            <img src="../../screenShots/MarkushEnumDialog.png">
            <br>

        </p>

        <h4>Structures</h4>
        <p>Structures are displayed in 3 regions of the window:</p>
        <ol>
            <li><strong>Markush structure:</strong> This is displayed in the top left. The Markush structure that
                you selected from the structure table is displayed, but you can edit this by double clicking on
                the panel.</li>
            <li><strong>Query structure:</strong> This is displayed in the panel to the right of the Markush structure.
                It may be empty if no structure search has been run. You can edit this by double clicking on
                the panel.</li>
            <li><strong>Enumerated structures:</strong> These are displayed in the main panel to the bottom of the
                window and will initially be empty. Once you have enumerated some structures each one can be opened
                by double clicking on it. THis opens Marvin Sketch and allows access to the full Marvin functionality.
                For instance, you can continue enumeration in the case of partially enumerated structures, or you
                can calculate molecular properties for any particular structure.</li>
        </ol>

        <h4>Enumerate tab</h4>

        <p>The Markush Enumeration dialog operates in 3 different modes which can be specified on the 
            'Enumerate' tab:</p>
        <dl>
            <dt>Full enumeration</dt>
            <dd>This performs exhaustive enumeration of the Markush structure.
                Markush libraries can potentially be vast in size (bigger that the
                number of atoms in the universe!), so the enumeration is limited to a
                maximum number of structures that you can specify. By default this is
                set to 100 structures.
            </dd>

            <dt>Random enumeration</dt>
            <dd>This performs random enumeration of the Markush structure. This
                is most useful for large Markush libraries where it is not practical to
                fully enumerate the library. Random enumeration allows you to sample
                the library in a random fashion so that you obtain a good representation
                of the various structures in the library.
                The same warning about library size that are described for full enumeration
                also apply to random enumeration.
            </dd>

            <dt>Markush reduction according to the hit</dt>
            <dd>This option is only active when you have run a substructure search
                on the Markush table and when you have a Markush Search license.
                In this mode the enumerated structures are limited to those that contain
                the substructure.
                Whilst this usually significantly reduces the number of enumerated
                structures, the limits on the enumerated library size still apply.
                You can see the part of the enumerated structure that corresponds
                to the query substructure using the typical hit display options.
                Note that multiple enumerated paths matching the substructure may
                result in the same enumerated structure, so the results may contain
                duplicates.
            </dd>
        </dl>
        <p><strong>Expand homology groups:</strong>
            This options specifies whether homology groups (Alkyl, Aryl etc.)
            in the Markush structure should be enumerated with examples of the group. Note that this gives a
            representative sample of structures for the homology group, but not necessarily an exhaustive set.</p>

        <p><strong>Max Structures:</strong>
            This limits the number of enumerated structures that are generated.</p>

        <h4>Display tab</h4>
        <p>This specified display options for the enumerated structures.

        <p><strong>Alignment:</strong>
            Are enumerated structures aligned to the Markush core (full or random enumeration) or
            to the query structure (Markush reduction).</p>

        <p><strong>Colouring:</strong>
            Are enumerated structures coloured according to their R-groups (full or random enumeration) or
            to the query structure (Markush reduction).</p>

        <p><strong>Show R-groups:</strong>
            Are R-groups displayed in the Markush structure and the enumerated structures.</p>

         <p> Display tab:
            <img src="../../screenShots/MarkushDisplay.png">
        </p>

        <h4>Filter tab</h4>

        <p>This allows a Chemical Terms filter to be specified that is applied to each enumerated structure.
            Those enumerated structures that fail the filter are discarded. When a filter is set you may then
            see fewer structures than you set as the maximum as some may be discarded. To set a Chemical Terms filter
            double click on the panel and the editor opens. This is the same editor that is used when specifying a
            <a href="../managing_data/queries/query_builder.html#chemterms-filter">Chemical Terms filter for queries</a>,
            and the filter specified must return a boolean value (true or false). This filter can be useful for filtering
            out non-drug-like structures or similar purposes. Note that it only applies to Full or Random enumeration
            as Markush reduction can generate partially enumerated structures for which properties cannot be calculated.
            For more information about specifying Chemical Terms filters see
            <a href="https://www.chemaxon.com/chemterms.html">here</a>.
        </p>

         <p> Filter tab:
            <img src="../../screenShots/MarkushFilter.png">
        </p>
       
        <h4>Output tab</h4>

        <p>This tab lets you specify how you want your enumerated structures output. The default is to output them to
            the display area in the lower half of the window, in which case you can also specify the grid size.
            Alternatively you can output to a file. When this option is set you are prompted for the filename when
            you start the enumeration.</p>

        <h4>Other features</h4>


        <p><strong>Library size</strong>
            The full enumerated size of the library is displayed beneath the Markush structure. This helps you
            decide whether to use full or partial enumeration, and whether to
            adjust the limit on the maximum library size. Note: the actual number
            of enumerated structures may be less than the calculated full enumerated
            library size. This is because the actual enumeration includes a valence filter
            that excludes incorrect structures. For instance this can happen when
            using query bond features e.g. an ANY bond attached to a benzene ring
            will give a predicted library size of 3, but when the actual enumeration
            is performed only a single structure will be generated as the double and
            triple bond variants would result in valence errors.</p>

        <h4>Performing enumeration</h4>
        <p>Once the appropriate options have been set the enumeration can be started 
            by pressing the 'Enumerate' button. Once running this button changes to 'Cancel'
            allowing the enumeration to be halted at it's current position. Results are
            displayed as the structures are generated. If enumerating to file sample
            enumerated structures are displayed as the enumeration proceeds.
        </p>


        <p><strong>Memory usage</strong>: Enumerated libraries can be very large. Enumeration can be slow and use
            lots of memory. If you are wanting to enumerate large libraries then consider: </p>
        <ol>
            <li>Increasing the amount of memory available to Instant JChem. See the
                <a href="../tips_and_tricks/memory_usage.html">memory usage documentation</a>
                for details.
            </li>
            <li>Outputting the results to file rather than displaying them.</li>
        </ol>


        <br>


                <br/>
                <hr/>
                <p style="font-size:xx-small;text-align:center">Copyright &#169; 1998-2013
                <a href='http://www.chemaxon.com' target='_top'>ChemAxon Ltd.</a>
                All rights reserved.</p>
                <br>

    </body>
</html>

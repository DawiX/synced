<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>

        <title>Instant JChem URLs</title>
        <link rel="StyleSheet" href="../../ijc.css" type="text/css">
    </head>
    <body> 
        <p style='text-align:center'><a href='../ijcTOC.html'>Table of Contents</a></p>
        <h2>Instant JChem URLs</h2>

        <h3>Overview</h3>

        <p>Instant JChem URLs is a feature that arrived in IJC 2.3. It allows you to define
            access to information in IJC using a URL that other people can use to obtain
            an identical view point. This is  best illustrated by means of an example.
            Lets assume that you have created a shared MySQL or Oracle database, and had
            created a specific list of compounds that you wanted other users to be able to use.
            You can easily do this by:</p>
        <ol>
            <li>Making the form public (sharing it) so that other people can
                access it
            </li>
            <li>Making the query public (sharing it) so that other people can
                access it</li>
            <li>Generating an IJC URL that specified the particular form and query</li>
            <li>Sending an email to the other users containing the URL</li>
            <li>The other users read the email, click on the link and IJC starts up,
                opens the form and executes the query. The user is now seeing exactly
                what you wanted then to see.</li>
        </ol>


        <p>For reasons that should be obvious IJC URLs are of most use in a shared project 
            environment where the URL has identical context for all users. 
            However they can also be used in local databases or in remote databases that
            are not part of a shared project configuration, but they are of more limited use.
        </p>

        <h3>Generating URLs</h3>
        <p>An IJC URL is generated using a dialog that can be opened with Tools -&gt; Generate URL.
            You must be using a shared project (otherwise the URL would have no meaning when 
            shared).
        </p>

        <!-- TODO - update screenshot  as there is new option to hide data trees -->
        <img src="../../screenShots/generate_url.png" alt="Generate URL dialog">
        <br>
        <br>

        <p>At the top of the dialog is the list of actions that are available. Opening a 
            view is currently the only action, but others may be added in the future.</p>

        <h4>Warnings: </h4> 
        <p>When generating URLs you may see some warnings about the URL you are trying to generate.</p>

        <dl>
            <dt>Selected view/query/list is not shared.</dt>
            <dd>If you are creating the generated URL for other 
                people to use then it is essential that any views, lists and queries that are
                used in the URL are shared. Without this your other users will not be
                able to access the information. If you try
                to use one of these that is not shared you will see a warning message 
                informing you of this. You can share the item by selecting the view/list/query 
                and clicking on the 'Share' button.
            </dd>

            <dt>URL will not work if you put it in a web page or email.</dt>
            <dd>This indicates that the shared project is not deployed in a manner that 
                allows you to use the URL in a web page or an email. The Java Web Start 
                instance of IJC must be deployed using the UrlWebApp server. To see how to 
                do this consult the 
                <a href="https://www.chemaxon.com/instantjchem/ijc_latest/docs/admin/ijc_urls.html">admin docs</a>.
            </dd>
        </dl>

        <h4>Creating an 'Open view' URL</h4>

        <p>Specify the exact action:</p>
        <dl>
            <dt><strong>Open view</strong></dt>
            <dd>This opens the specified view and shows all the data.
                <br>
                An example URL looks like this:
                <br>
                <code>http://www.chemaxon.com/urldemo/demo.jnlp?view=8BCD646043688090353F199D948541F8&action=OpenURLAction&project=UrlPubChemDemo&schema=.server%2Flocaldb</code>
                <br>
                Note: multiple views can be specified by manually editing the URL
                and adding the additional views as a comma separated list e.g.
                <br>
                <code>http://www.chemaxon.com/urldemo/demo.jnlp?view=8BCD646043688090353F199D948541F8,32F03682857288B598AEC37779F034AD&action=OpenURLAction&project=UrlPubChemDemo&schema=.server%2Flocaldb</code>
            </dd>

            <dt><strong>Open view and select query or list</strong></dt>
            <dd>This opens the specified view and applies the specified query or list.
                The query or list is specified using the right hand explorer. Only permanent
                lists or queries can be used. Temporary queries and lists are also shown
                as you can use the normal list and query operations to make these permanent
                (or to edit them or perform list logic etc. to generate new lists).
                <br>
                An example URL looks like this:
                <br>
                <code>http://www.chemaxon.com/urldemo/demo.jnlp?view=8BCD646043688090353F199D948541F8&action=OpenURLAction&project=UrlPubChemDemo&schema=.server%2Flocaldb&permanent_query=01B3200CCBF2AEB146421F9A13C644D4</code>
            </dd>

            <dt><strong>Open view and show rows</strong></dt>
            <dd>This opens the specified view and shows the specified rows. The rows
                that are specified are the CURRENT row numbers e.g. you can say open
                the view and show the rows that are currently rows 7, 11 and 18.
                This will open the view and show just those three rows.
                <br>
                An example URL looks like this:
                <br>
                <code>http://www.chemaxon.com/urldemo/demo.jnlp?view=8BCD646043688090353F199D948541F8&action=OpenURLAction&project=UrlPubChemDemo&selected_row=1999&schema=.server%2Flocaldb</code>
            </dd>
        </dl>

        <p>In all cases the view is specified using the 'Select view' explorer.</p>

        <p>There is also a checkbox named 'Show only selected data tree' which allows you to hide data trees that
            are not needed. This can be used to simplify what users see. Note that this should be considered as for
            convenience purposes, and is not a secure approach to hide data from users. If you need a secure approach
            to controlling access you should look at the approach described in the
            <a href="https://www.chemaxon.com/instantjchem/ijc_latest/docs/admin/tips/role_filtering/index.html">Filtering items using roles</a>
            administrators tips and trick.</p>

        <p>The generated URL is displayed at the bottom of the dialog. It can be generated
            in HTML format for pasting into a web page by checking the 'HTML text'
            checkbox. Access the URL by clicking on the 'Copy to clipboard' button and
            then paste it wherever you need.
        </p>

        <h3>Opening URLs</h3>


        <p>An IJC URL can be opened using any of these approaches: </p>

        <dl>

            <dt><strong>From inside IJC</strong></dt>
            <dd>Using Tools -&gt; Open URL and paste the URL into the text box.
            </dd>

            <dt><strong>Using a command line option</strong></dt>
            <dd>
                <code>instantjchem --openUrl &lt;url&gt;</code>
            </dd>

            <dt><strong>Using an extra parameter to Java Web Start URL</strong></dt>
            <dd>
                <code>http://www.yourserver.com/UrlWebApp/demo.ijp?view=32F03682857288B598AEC37779F034AD&action=OpenURLAction&project=demo&schema=.server%2Flocaldb&permanent_query=7BB2FB31A160E9874DCA06B4140E4292</code>
                <br>
                If you are generating the URL from the Java Web Start version
                then the full URL that launches IJC is generated, so the URL will
                launch IJC when pasted into a browser or clicked on in an email.
                Use the Generate URL function inside IJC to generate this - do not
                try to generate it manually!
            </dd>

        </dl>



        <h3>Additional information for administrators</h3>

        <p>IJC URLs have most benefit when IJC is launched using Java Web Start
            and requires the use of shared projects. Consult the following administrator's
            documentation for details of how to set these up:</p>
        <ul>
            <li><a href="https://www.chemaxon.com/instantjchem/ijc_latest/docs/admin/project_deploy.html">Deploying the Java WebStart version to your server</a></li>
            <li><a href="https://www.chemaxon.com/instantjchem/ijc_latest/docs/admin/jws_deploy.html">Deploying shared projects</a></li>
            <li><a href="https://www.chemaxon.com/instantjchem/ijc_latest/docs/admin/ijc_urls.html">How to access data in IJC using URLs</a></li>
        </ul>



                <br/>
                <hr/>
                <p style="font-size:xx-small;text-align:center">Copyright &#169; 1998-2013
                <a href='http://www.chemaxon.com' target='_top'>ChemAxon Ltd.</a>
                All rights reserved.</p>
                <br>

    </body>
</html>

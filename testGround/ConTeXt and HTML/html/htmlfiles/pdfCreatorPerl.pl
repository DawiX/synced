#!/usr/bin/perl
#
$html = $ARGV[0];
$output = $ARGV[1];



use HTML::HTMLDoc;
my $htmldoc = new HTML::HTMLDoc();
$htmldoc->set_input_file(\$html); # alternative to use a present file from your fs
my $pdf = $htmldoc->generate_pdf();
$pdf->to_file(\$output);

<html>
    <head>
        <title>Calculated Fields</title>
        <link rel="StyleSheet" href="../../ijc.css" type="text/css">
    </head>

    <body> 
        <p style='text-align:center'><a href='../ijcTOC.html'>Table of Contents</a></p>
        <h2>Calculated Fields</h2>

        <ul>
            <li><a href="#purpose">Purpose of calculated fields</a></li>
            <li><a href="#how">How are calculations are performed</a></li>
            <li><a href="#where">Where are calculations are performed?</a></li>
            <li><a href="#values">How values are provided to the script?</a></li>
            <li><a href="#uptodate">Are the results always up to date?</a></li>
            <li><a href="#othertables">What about values from other tables?</a></li>
            <li><a href="#returnvalue">How are values returned from the script?</a></li>
            <li><a href="#structures">How do I handle structures?</a></li>
            <li><a href="#errors">What about errors?</a></li>
            <li><a href="#helperfunctions">Helper functions</a></li>
            <li><a href="#addNew">To add a new calculated field</a></li>
            <li><a href="#edit">To edit an existing calculated field</a></li>
            <li><a href="#examples">Examples</a></li>
        </ul>

        <p>
            Calculated fields are fields whose values are calculated from the value(s) of other field(s)
            and possibly external sources..
        </p>
        <p>
            IJC also has three other types of field:
        </p>
        <ul>
            <li><a href="fields_standard.html">Standard Fields</a> (for normal data)</li>
            <li><a href="fields_chem_terms.html">Chemical Terms Fields</a> (for structure based predictions)</li>
            <li><a href="fields_url.html">URL Fields</a> (for incorporating data located outside IJC)</li>
        </ul>



        <h3><a name="purpose"></a>Purpose of calculated fields</h3>

        <p>Calculated fields allow calculations to be performed on data from other fields.
            A simple example would be to calculate the sum of two fields (e.g. "A + B" like
            calculations) though more complex calculations such as averaging values from a
            different table, or even accessing data from outside IJC are potentially possible.
        </p>
        <p>We have tried to ensure that creating simple calculations is simple, but that you have
            the flexibility to generate much more complex calculations if you need. This is done by
            the calculation being a script that is evaluated, so in effect it is a program that can be
            used to do whatever you want. However for simple calculations such as the A + B example
            mentioned above, the syntax to use can be as simple as A + B.
        </p>

        <h3><a name="how"></a>How are calculations are performed</h3>
        <p>As mentioned above, calculations are in fact scripts that are executed. The language for the
            scripts is
            <a href="http://groovy.codehaus.org/">Groovy</a>,
            a dynamic programming language that is closely linked to the Java language that most of IJC
            is written in.
            Groovy provides a powerful programming environment, but also a simple syntax that allows
            calculations to be defined in a way that looks very natural. Look at the documentation on
            the Groovy web site for more details. There are also
            <a href="http://groovy.codehaus.org/Books">some books</a> available.
            For advanced usage you definitely need to understand Groovy, but for simple cases you should
            just be able to follow the examples described here.
        </p>

        <h3><a name="where"></a>Where are calculations are performed?</h3>
        <p>Calculations are performed in IJC itself not in the database. Results of the calculation are not
            stored in the database, but are calculated dynamically as they are needed. Once consequence
            of this is that calculated fields are not searchable or sortable. If this is a problem
            then you have two possible solutions:</p>
        <ol>
            <li>Instead of using an IJC calculated field, create the data directly in the database, by
                means of a view or trigger. You will need a database expert to do this.</li>
            <li>Materialize the results of the calculation in the database using the 'Convert type'
                function. A new field that does store its values in the database will be created and the current
                values of the calculated field will be written to that database column. Do note however that
                the values of the new field will not be updated if the dependent values change or new rows
                are added.</li>
        </ol>

        <h3><a name="values"></a>How values are provided to the script?</h3>
        <p>Let's start with a simple A + B example. You have two fields in your entity and want to add a new field
            that is the sum of the values of those two fields. To do this you specify the two fields in step 2
            and specify that the variable names are A and B.
            Then when the calculation is performed these two variables are magically created in the script
            context and can be used in the script. So you can write a script that uses these. And really, the
            script can just be:</p>
        <pre>A + B</pre>
        <p>Its that simple (but see more below).</p>
        <p>In testing your test variables are injected as the values for A and B, whilst when running the actual
            values for your two fields are injected for each row you are calculating.</p>
        <p>What is happening is that Groovy is executing the script "A + B" and A has the value of your first field
            and B the value of the second field. The script could be as simple as "A + B" or it could be
            hundreds of lines of Groovy code.</p>

        <h3><a name="uptodate"></a>Are the results always up to date?</h3>
        <p>We hope so, but if you are not sure use 'Data' -> 'Reload data' to be sure. This will force the
            calculations to be re-run.</p>

        <h3><a name="othertables"></a>What about values from other tables?</h3>
        <p>Values for fields from other tables (IJC entities) can also be specified, and this really demonstrates the power
            of calculated fields in IJC. But you might wonder how this works, particularly in the case of
            one-to-many relationships when there will be multiple values for a field in the other table.
            You might want to generate a summary of these values (e.g. the average value), or to combine
            values from multiple fields into a single result (e.g. conditionally averaging values from one
            field based on the value of another field from that entity. All of this is possible, and much
            more. You can define how these values are passed to the script. To do this you use the options in
            the 'Type' column of the variables table. The default is single, which means that just a single
            value will be passed. In the case of a field having multiple values you can choose to pass the values
            as LIST in which case all values will be passed (as a Java List object). Alternatively the multiple
            values can be converted to a single value and passed using one of the other options, SUM, AVG, STR_CAT,
            which generate the sum, average and comma separated string of the values accordingly. All of those
            operations could also be performed by the script if you passed the value as LIST.
            In the simple case you can just use one of these functions e.g. use the SUM function and then the
            script will just receive a value for that variable that is the sum of those values.
        </p>

        <h3><a name="returnvalue"></a>How are values returned from the script?</h3>
        <p>Groovy has a nice trick that the last statement is automatically returned as the result. So in the
            case of "A + B" it is the result of that addition that is returned, which is exactly what you want in
            this case. However in some cases this may not suffice. If so then you can provide an explicit return
            value like this:</p>

        <pre>
            C = A + B
            return C
        </pre>

        <p>If you are not seeing the results you expect try adding a "return X" statement as the last line.</p>

        <h3><a name="structures"></a>How do I handle structures?</h3>
        <p>The structure field can be used a stript variable. The data type is the Structure.java IJC class.
            The key thing you will probably want to do with is is get the structure in either its original format
            (e.g. smiles, SDF...) or as  chemaxon.struct.Molecule object. Here are examples of each (both assume the
            structure field is defined as the 'mol' variable.</p>

        <p>1. As the original format. Here we are calling the getEncodedMol() method of the Structure class.
        </p>
        <pre>
        mol.encodedMol
        </pre>


        <p>2. As a Marvin Molecule object. Here we are calling the getNative() method of the Structure class to obtain
            the Molecule object, and then calling the toFormat() method on the Molecule. You can do much more with
            the Molecule using the Marvin API.
        </p>
        <pre>
        def m = mol.native
        m.toFormat("smiles") // convert to smiles
        </pre>

        <p>In both these cases a text value is generated (e.g. you would be using a calculated text field).
            To display this as a structure you can use the structure renderer in the grid view or the molecule
            widget in the form view.</p>
        <p>For more information see these links:</p>
        <ul>
            <li><a href="http://www.chemaxon.com/instantjchem/ijc_latest/docs/developer/api/com-im-df-api/index.html">IJC Structure class</a></li>
            <li><a href="http://www.chemaxon.com/marvin/help/developer/beans/api/chemaxon/struc/Molecule.html">Marvin Molecule class</a></li>
        </ul>

        <h3><a name="errors"></a>What about errors?</h3>
        <p>Your script might generate errors. It may be a bad script and fail for every execution, or it might
            fail in some specific cases e.g. divide by zero errors, or errors resulting from missing values.
            Because of this you must test you expression before it can be saved, so that at least we know the
            syntax is valid. If errors do occur then they are consumed and the result will be
            null (empty). if you see empty results then it could be because of errors from the script for
            particular values. Null (missing) values are a particular cause of this, and you can guard for this
            in your script (but see below on the helper functions) or you can specifically catch errors in your
            script using <code>try ... catch ... </code> blocks, but this is too advanced for this guide and
            you should consult the Groovy docs for details.</p>

        <h3><a name="helperfunctions"></a>Helper functions</h3>
        <p>There is a hidden "gotcha" in the "A + B" example. Whilst it works fine when values are defined for
            A and B, the script will cause an error when either A or B are null (missing values). Although these
            errors will be handled and you will end up with empty values, this is not an ideal solution. So to
            handle this we provide some "helper" functions in the script that make things easier. Theses are
            functions like sum(), minus(), multiply(), divide(), avg(), concat() that can be used to simplify
            writing functions that are more error tolerant. A full list is described in the following table.
            But as a simple example you could better replace your <code>A + B</code> script with
            <code>sum(A, B)</code>, and instead of using the AVG function to average your values that are
            passed to the script, you could instead pass as LIST and then do the averaging like this:
            <code>avg(A)</code>.
        </p>

        <table cellpadding="3" border="1">
            <tr><th>Function</th>
                <th>Description</th>
                <th>Examples</th>
            </tr>
            <tr>
                <td>sum()</td>
                <td>Adds numbers or lists of numbers</td>
                <td><code>sum(2, 3, 4) // equals 9<br>
                        sum(a, b) // a and b are variables containing numbers<br>
                        sum(c) // c is a list containing numbers</code>
                </td>
            </tr>
            <tr>
                <td>multiply()</td>
                <td>Multiplies numbers or lists of numbers</td>
                <td><code>multiply(2, 3, 4) // equals 24<br>
                        multiply(a, b) // a and b are variables containing numbers<br>
                        multiply(c) // c is a list containing numbers</code>
                </td>
            </tr>
            <tr>
                <td>minus()</td>
                <td>Subtracts the second number from the first</td>
                <td><code>minus(43, 22) // equals 21<br>
                        minus(a, b) // a and b are variables containing numbers</code>
                </td>
            </tr>
            <tr>
                <td>divide()</td>
                <td>Divides the first number by the second</td>
                <td><code>divide(44, 11) // equals 4<br>
                        divide(a, b) // a and b are variables containing numbers</code>
                </td>
            </tr>
            <tr>
                <td>avg()</td>
                <td>Calculates the average of numbers.<br>null values are ignored.</td>
                <td><code>avg(2, 3, 4) // equals 3<br>
                        avg(a, b) // a and b are variables containing numbers<br>
                        avg(c) // c is a list containing numbers</code>
                </td>
            </tr>
            <tr>
                <td>concat()</td>
                <td>Joins values with the specified characters.</td>
                <td><code>concat(":", 1, 2) // equals "1:2"<br>
                        concat("
", a, b) // joins the a and b variables with the newline character<br>
                        concat(",", c) // joins the list of values in the c variable with a comma</code>
                </td>
            </tr>
            <tr>
                <td>each()</td>
                <td>Coordinated looping through 2 or 3 lists.</td>
                <td><code>each(a, b) { x, y -> <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;// do something with x and y<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;// x and y will be a[0] and b[0] for first iteration<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;// x and y will be a[1] and b[1] for next iteration<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;// etc. until one of the lists runs out of values<br>
                        }<br><br>
                        each(a, b, c) { x, y, z -> <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;// do something with x, y and z<br>
                        }
                    </code>
                </td>
            </tr>
        </table>

        <p>In the examples in this table the // character indicates the start of a comment. This and the
           text to the right of it does not form part of the function, its just there to help describe it.

           The detailed javadoc for a java class implementing the above functions can be found
           <a href="http://www.chemaxon.com/instantjchem/ijc_latest/docs/developer/api/com-im-df-api/com/im/df/api/support/CalculationsScriptBaseClass.html">here</a>.
        </p>
         <p>Note also that there is comprehensive support from the scripting language itself for all sorts
            of operations. e.g. the
            <a href="http://download.oracle.com/javase/6/docs/api/java/lang/Math.html">java.lang.Math</a>
            class provides a wide range of mathematical operations,
            including logarithm, trigonometry and other functions. e.g. to generate the base 10 logarithm
            of a variable x use:<br>
            <code>Math.log10(x)</code>.
        </p>

        <p>We welcome suggestions for additional helper functions.</p>



        <h3><a name="addNew"></a>To add a new calculated field:</h3>

        <p>A new calculated field can be added by any of these methods: </p>
        <ul>
            <li>In the Schema Editor
                <ul>
                    <li>Using the new calculated field icon in the toolbar
                        (&nbsp;<img src="../../icons/field-calc-new.png" alt="calculated field icon">&nbsp;)</li>
                    <li>Using the appropriate item in the popup menu that appears when you right click on
                        an entity, or other appropriate places.</li>
                </ul>
            </li>
            <li>Using the 'New Calculated Field...' item from the Data menu</li>
            <li>In the
                <a href='javascript:void(0)' onclick="window.open('../keywords/grid_view.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Grid View</a>
            </li>
            <li>In Design mode of the
                <a href='javascript:void(0)' onclick="window.open('../keywords/form_view.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Form View</a>
            </li>
        </ul>
        <p>
            Note: starting to create other fields is also done in the same way as calculated fields. See the
            documentation for standard fields for full details.
        </p>
        <br><br>
        <img src="../../screenShots/calc-field-dialog.png" alt="Add calculated field dialog">
        <br><br>

        <p><strong>Step 1. Specify field type: </strong></p>

        <p>Specify the type of field (Integer, Text etc.) from the first combo box. The is the type of
            value for the calculation result.</p>

        <p><strong>Step 2. Specify the required fields: </strong></p>

        <p>Click on the 'Add' button and specify the fields the calculation depends upon. For each row of the table
            the calculation will be performed as needed, and the values for these required rows will be "injected"
            into the calculation as named variables, so that those variables are available for evaluation.
            You can also specify the entity the field comes from. The default is fields from the entity to which
            you are adding the calculated field, but you can also choose any entity that has a relationship to that
            entity.</p>
        <p>
            You can specify the name of the variable for the selected field (a default is generated for you
            based on the name of the field). This is the variable name in the script that will contain the
            actual value to be used.</p>
        <p>
            You can also specify how the value(s) for the field are to be presented to the calculation. See above
            for more details of this.</p>
        <p>
            A value for testing can also be specified. Again a default is provided but
            you can specify different ones to be sure that your calculation is giving the expected results.
        </p>
        <p>Do this for each of the fields your calculation is going to use.</p>

        <p><strong>Step 3. Specify the expression for the calculation: </strong></p>

        <p>Enter the expression for the calculation. See below for examples.</p>

        <p><strong>Step 4. Test the calculation: </strong></p>
        <p>Check that the calculation works as expected using the 'Validate' button.
            You might need to change the test variables to run a more meaningful test.
            This lets you check that the expression is valid and that you get the expected results.
        </p>

        <p><strong>Step 5. Create the calculated field: </strong></p>
        <p>Click on the OK button to add the calculated field.</p>


        <h3><a name="edit"></a>To edit an existing calculated field:</h3>
        <p>The expression for an calculated field can be updated, either in the schema editor or in the
            widget customiser in the form or grid view. The process is much the same as the later steps of
            creating a new field so won't be repeated here.</p>

        <h3><a name="examples"></a>Examples</h3>
        <p>The best way to illustrate all this is with some real examples. So load up the demo project data
            and look through these examples.</p>
        <p>These examples provide guides to how you can use calculated fields. They use the demo data that comes 
            with IJC so that they can be easily followed. They are not necessarily meaningful calculations and you
            not doubt will have more meaningful ones to apply to your own data.</p>

        <h4>1. Simple A * 1000 example</h4>
        <p>Here we will start with the most simple example: take the value of a field and multiply it by 1000.
            This could be useful for converting units. e.g. grams into milligrams. In our example we will multiply
            the molecular weight of the Pubchem demo structures by 1000.</p>

        <ol>
            <li>Open the pubchem grid view.</li>
            <li>Click on the 'New calculated field' icon in the grid view toolbar. The dialog will open.</li>
            <li>Select 'Calculated Decimal Number Field' as the type of field, as the results of out calculation
                will be a decimal number.</li>
            <li>Enter "MW * 1000" as the field name</li>
            <li>Click on the 'Add' button and select the 'Mol Weight' field.</li>
            <li>Enter "molWeight * 1000" as the expression.</li>
            <li>Click 'Validate' and you should see the result or 1000.0. This is correct because the default
                test value is 1.0</li>
            <li>Enter 123.45 as a more meaningful test value and check you get the right answer.</li>
            <li>Click OK and the calculated field will be added to the grid view and you will see the results
                of the calculations.</li>
        </ol>

        <p>OK, so that was pretty simple. But actually its not ideal as the calculation is subject to errors
            resulting from missing values. If for some reason the molecular weight is undefined (e.g. the
            molecule contains list atoms) then the calculation would fail for those, and although the error
            would be handled and the result would be empty as expected its not the best approach.
            A better approach would be to use the multiply() helper function to do the calculation as that correctly
            handles null values. So to do this edit the field definition and change the calculation to
            "multiply(molWeight, 1000)"</p>


        <h4>2. Simple A / B example</h4>
        <p>Here we use two fields in a calculation. Still its a pretty simple calculation and very simple to
            perform. We won't repeat the steps in detail form now on, just the key aspects. In this example
            we will divide the molecular weight by the XLogP value.</p>

        <ol>
            <li>Add the field as before and add the Mol Weight and XLog P fields as variables</li>
            <li>Enter "divide(molWeight, xLogP)" as the expression (assuming you did not rename the variables).</li>
            <li>Validate and add the field.</li>
        </ol>


        <h4>3. Using values from different table</h4>
        <p>Here we will switch to the Wombat demo data and add a calculated field to the structure entity whose
            results are a summary of the corresponding rows in the assay table. In this case we will generate
            the average of the VALUE.MIN field.</p>

        <ol>
            <li>Create a new form view for the 'Wombat (compound view)' data tree and add two table widgets to the form, one for
                the structure entity and one for the assay entity. Make sure you are showing the VALUE.MIN and
                VALUE.MAX fields in the assay table.</li>
            <li>Switch to design mode and add a new calculated field to the structure table</li>
            <li>Choose 'Calculated Decimal Number Field' as the field type and add the VALUE.MIN field
                from the assay entity to the variables table. You will probably want to change the variable name.
                Use 'vmin'.</li>
            <li>Change the 'Type' to AVG.</li>
            <li>Enter vmin as the expression (this is necessary as we need to specify what the script should
                return as its result.</li>
            <li>Validate the expression and add the field.</li>
        </ol>

        <h4>4. Using the avg() helper function instead</h4>

        <p>The last example used the AVG function to average the values BEFORE they are set to the script. Let's see
            how we could instead have done the averaging in the script. Instead of using AVG as the TYPE
            chose LIST. This will pass the individual values as a list. For the expression specify
            "avg(vmin)" This uses the avg() helper function to do the averaging.</p>


        <h4>5. Doing the averaging ourselves</h4>
        <p>Now lets look at doing the averaging ourselves as an example of when we need to do the actual work in
            the script (let's assume that neither of the above approaches could be used and we needed to calculate
            the average ourselves).</p>

        <p>Do as before and add the vmin variable as LIST. Then enter this as your expression:</p>
        <pre>
sum = 0
count = 0
vmin.each {
    sum += it
    count++
}
return count ? sum / count : 0
        </pre>

        <p>OK, so at last we have something that looks like a program. You would only need to do this for more
            advanced calculations, so you may want to ignore this, at least until you are more comfortable with
            the simpler calculations. But lets look at this example briefly:</p>


        <table cellpadding="3" border="1">
            <tr>
                <td><pre>sum = 0
count = 0</pre></td><td>This initialises two variables that we will use to record the running total and the
                    number of values </td>
            </tr>
            <tr>
                <td><pre>vmin.each {
    ...
}</pre></td><td>This iterates through the item in the vmin list using the Groovy each operator</td>
            </tr>
            <tr>
                <td><pre>    sum += it
    count++</pre></td><td>This happens inside the loop and adds the value (exposed as the magical 'it' variable)
                    to the sum and increases the count by 1
                </td>
            </tr>
            <tr>
                <td><pre>return count ? sum / count : 0</pre></td><td>This returns the result, with special handling
                    for the case of having no values to avoid divide by zero errors.
                </td>
            </tr>
        </table>

        <p>OK, so that's a little more advanced that we probably need for now, but it begins to show the flexibility
            that is possible.</p>

        <h4>6. Generating text summaries</h4>
        <p>Here we will generate a text field that summarises the values in the assay table. Here we will write
            out the values from the VALUES.MIN field as a text field, one value per line.</p>

        <p>Do as before, and create a 'Calculated Text Field' and add the VALUE field as LIST with the
            val variable name. Now enter this as the expression: <code>concat('
', val)</code>.
            Here we are using the concat() helper function, for which the first argument is the separator to
            use, '
' in this case being the newline character. Pretty simple really.


        <h4>7. Summarising multiple fields.</h4>
        <p>This is similar to the previous example, other than we will summarise data from two fields, VALUE.MIN and
            VALUE.MAX. We will use the each() helper function that allows us to loop through multiple lists in
            coordinated fashion.</p>
        <p>As before, create a text field and add VALUE.MIN and VALUE.MAX as the vmin and vmax variables. Now use
            this as the expression:</p>
        <pre>
res = ""
each(vmin, vmax) { a, b ->
  res += concat(' - ', a, b)
  res += '\n'
}
return res
        </pre>
        <p>The only thing really new here is the each() helper function. You pass it two lists and for each iteration
            it returns the two values at that position in the list. e..g first time through the loop you get the first
            items in the lists, second time through the second items etc. The values are passed in as the a and b
            variables specified in the <code>a, b -></code> part of the expression. the inner part of the loop
            just combines the values of a and b into the resulting string.</p>

        <h4>Summary</h4>
        <p>These examples have hopefully shown how simple calculations can be very simple to perform, but that you
            also have the capability to perform much more complex "calculations". In fact they don't really
            have to be calculations at all. They could query a different database or web service to obtain data and
            make it accessible to IJC. Examples for these sorts of things will be provided later.</p>
        <p>


        <p>
            We welcome feedback on this feature. Please provide feedback via the
            <a href="https://www.chemaxon.com/forum/forum62.html">Instant JChem forum</a>.
        </p>


        <br>
        <br>


                <br/>
                <hr/>
                <p style="font-size:xx-small;text-align:center">Copyright &#169; 1998-2013
                <a href='http://www.chemaxon.com' target='_top'>ChemAxon Ltd.</a>
                All rights reserved.</p>
                <br>

    </body>
</html>

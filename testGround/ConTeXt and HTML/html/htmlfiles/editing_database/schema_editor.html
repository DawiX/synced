<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>

        <title>Ways of editing your Schema</title>
        <link rel="StyleSheet" href="../../ijc.css" type="text/css">
        
        
        <style>
            PRE { background-color: silver; }
        </style>
        
    </head>
    <body>
        <p style='text-align:center'><a href='../ijcTOC.html'>Table of Contents</a></p>
        <h2>The Schema Editor</h2>
        
         <ul>
            <li><a href="#overview"> Overview </a></li>
            <li><a href="#items"> Items </a></li>
            <li><a href="#operations"> Operations</a></li>
            <li><a href="#creating"> Creating </a></li>
            <li><a href="#deleting"> Deleting </a></li>
            <li><a href="#promoting"> Promoting </a></li>
            <li><a href="#schemas"> Schemas </a></li>
            <li><a href="#security"> Security </a></li>
            <li><a href="#connection"> Connection </a></li>
            <li><a href="#property-tables"> Property tables</a></li>
            <li><a href="#dbschemas"> Database schemas</a></li>
            <li><a href="#miscellaneous"> Miscellaneous </a></li>
            <li><a href="#entities"> Entities </a></li>
            <li><a href="#fields"> Fields </a></li>
            <li><a href="#relationships"> Relationships </a></li>
            <li><a href="#tables"> Tables </a></li>
            <li><a href="#indexes"> Indexes </a></li>
            
        </ul>
        
        <p>The Schema
            <a href='javascript:void(0)' onclick="window.open('../keywords/schema_editor.html','Definition', 'width=300,height=300,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')">Schema Editor</a>
            is the expert tool for managing your IJC database. Most simple operations can be
            performed without needing to use the Schema Editor, but in some cases it may be
            easier to use the Schema Editor, and some advanced operations can only be performed
            using it.
        </p>
        <p>The Schema Editor allows you to manage your current IJC Entities and Fields, as well
            as to add any existing database artifacts that are not yet known to IJC. In this way
            it lets you incorporate data from an existing database into IJC. Since IJC 3.0 the
            functionality that used to be present in the Data Tree Editor was moved to the
            Schema Editor and the Data Tree Editor is not longer present.
        </p>

        <a name="overview"></a><h3>Schema Editor overview</h3>

        <p>The Schema Editor contains an explorer component on the left and an editor
            component on the right. In simple terms you select the appropriate item in
            the explorer, and make changes by one of these approaches:
        </p>

        <ul>
            <li>Making changes in the editor and clicking the Apply button</li>
            <li>Using the buttons in the Schema Editor toolbar</li>
            <li>Using the right click popup menu which is present in many places</li>
        </ul>

        <p>The explorer panel on the left has 5 tabs:</p>
        <ul>
            <li><strong>Schema</strong> - properties and settings for the schema itself</li>
            <li><strong>Data Trees</strong> - A list of the data trees in the schema and the data
                hierarchy of each data tree</li>
            <li><strong>Entities</strong> - each entity in the schema, plus the fields and relationships
                for that entity.</li>
            <li><strong>Views</strong> - details of the database views</li>
            <li><strong>Tables</strong> - details of the database tables</li>
        </ul>

        <p> Explorer panel:
            <img src="../../screenShots/ExplorerPanel.png">
        </p>

        <p>Each outlook bar shows the items relevant to that type in the explorer as top level nodes.
            The child nodes for each item depend on the type of item.
        </p>

        <br>
        <img src="../../screenShots/schema_editor.png">
        <br><br>

        <a name="items"></a><h3>Explorer contents</h3>

        An item in IJC usually represents something in the database. For instance:
        <ul>
            <li>An Entity represents a database table</li>
            <li>A Field represents a database column</li>
            <li>A Relationship represents a foreign key constraint and is used to define
                the relationship between two Entities</li>
        </ul>
        <p>
            When an object is created in IJC the database artifact is created and IJC is made aware
            of this together with any additional data that is needed about the item. But what happens if
            the database artifact has not been created by IJC? In this case IJC will 
            not possess the additional information it needs about it to be able
            to manage it. This may be the case if you have connected to a database that already has
            tables in it that were created by other means. The Schema Editor lets you see these artifacts
            and &quot;promote&quot; them into IJC so that it can manage them. When you do this
            you need to provide the missing information to IJC so that it knows how to handle them.
        </p>


        <a name="operations"></a><h3>Performing operations in the Schema Editor</h3>

        <p>There are a number of general approaches to performing operations in the
            Schema Editor that apply to most item types.</p>

        <ul>
            <li>Locate the item in the appropriate explorer, right click on it and
                choose the appropriate option from the popup menu. Examples:
                <ul>
                    <li>Locate an Entity in the 'Entities' tab, right click on it and select
                        'Delete' from the popup menu.</li>
                    <li>Locate a table in the 'Tables' tab, right click on it and select
                        'Promote to Entity'.
                    </li>
                </ul>
            </li>
            <li>Locate the container for the promoted item type in the appropriate
                explorer, right click on it and choose the appropriate option from the popup menu.
                The container can either be a container node, such as the Fields container
                of the Entity node, or for the top level elements it is the empty space 
                at the bottom of the explorer. Examples:
                <ul>
                    <li>Right click on the 'Fields' container of the Entity node,
                        right click on it and select 'New standard field...' from the popup menu.
                    </li>
                    <li>Right click on the empty space in the explorer of the 'Entities' tab
                        and select 'Promote table' and choose from the list of un-promoted tables
                        that are presented.
                    </li>
                </ul>
            </li>
            <li>Select the item in the appropriate explorer and use the appropriate
                icon in the Schema Editor toolbar. Examples:
                <ul>
                    <li>Select a table in the 'Tables'
                        tab and click on the 'Promote to Entity'
                        (&nbsp;<img src="../../icons/entity-generic.png">&nbsp;)
                        button in the toolbar.
                    </li>
                    <li>Select the 'Fields' container of an Entity node and click on
                        the 'New standard field...' button
                        (&nbsp;<img src="../../icons/field-standard-new.png">&nbsp;)
                        in the toolbar.
                    </li>
                </ul>
            </li>
        </ul>

        <a name="creating"></a><h3>Creating artifacts</h3>

        <p>The most common approach in IJC is to create a new IJC item simultaneously
            creating the corresponding artifact in the database. The following types can
            be created in the Schema Editor:
        </p>
        <ul>
            <li>Create an Entity along with its corresponding database table(s)</li>
            <li>Create a Field along with its corresponding database column(s)</li>
            <li>Create an Relationship along with its corresponding Foreign Key
                (and for many-to-many relationships a join table)</li>
            <li>Add an index to a database table, specifying the column(s) for the index</li>
        </ul>

        <p>Once you commence the 'create' process a dialog or wizard will appear to guide you
            through the process.
        </p>


        <a name="deleting"></a><h3>Deleting artifacts</h3>

        <p>An item in IJC schema can usually be deleted. Deletion can have two different
            meanings:
        </p>
        <ul>
            <li>Remove the item (e.g entity) from IJC but leave the underlying item (e.g table)
                in the database.</li>
            <li>Remove the item from IJC and also delete the underlying item from the database.</li>
        </ul>

        <p>
            Both of these are supported, and you choose which option you want when
            you are asked to confirm the deletion. IJC remembers your last choice for this
            setting (prior to IJC 5.3.2 delete from DB was checked by default). Choose wisely!
        </p>

        <a name="promoting"></a><h3>Promoting artifacts</h3>

        <p>When you have an artifact that is not yet part of an IJC schema you may be able
            to promote it into IJC. The following can be promoted:
        </p>
        <ul>
            <li>Database tables to Entities</li>
            <li>Database views to Entities</li>
            <li>Columns to Fields</li>
            <li>Foreign Keys to Relationships</li>
        </ul>


        <p>Once you commence the 'promote' process a dialog or wizard will appear to guide you
            through the process. The process of connecting IJC to an existing database
            is described in more detail in the
            <a href="existing_schemas.html">Using existing database tables</a>
            help page.
        </p>


        <a name="schemas"></a><h3>Manage Schema</h3>

        <p>The schema tab contains properties for the schema itself. Select the appropriate category to see and edit
            the settings:</p>

        <a name="security"></a>
        <p><strong>1. Security</strong></p>
        <p>You can define or edit the security settings here. See the
            <a href="../security/changing_security_settings.html">Changing Security Settings</a>
            documentation for more details.</p>

        <a name="connection"></a>
        <p><strong>2. Connection</strong></p>
        <p>Properties of the database connection.</p>

        <a name="property-tables"></a>
        <p><strong>3. JChem property tables</strong></p>
        <p>A JChem database needs a Property Table which is needed to store various
            information about the JChem structure tables. By default a single property table
            named JCHEMPROPERTIES is created when the IJC schema was originally created,
            and this is sufficient for most purposes. However it is possible to use
            a different table name, or to to use multiple property tables.
            Note: this is not recommended unless you have a specific reason for needing
            to do so.</p>
        <p>To manage JChem property tables do the following:</p>
        <ol>
            <li>Switch to the 'Tables' tab and click on the 'Manage JChem property tables' icon
                ( <img src="../../iconImages/manage-properties.png"> ) in the Schema
                Editor toolbar. The editor will open.</li>
            <li>Add or delete a propety table using the appropriate buttons. Note:
                you can only delete a property table if it does not currently manage any
                JChem tables. Delete the JChem tables before deleting the property table.</li>
        </ol>

        <p><strong>Important:</strong> Do not delete JChem tables directly from the database
            or the information in the property tables will become inconsistent. Always delete
            them using IJC which will ensure that the property table is updated appropriately.
            Property tables can also be managed using other ChemAxon tools such as JChem Manager.
            However, avoid doing this for databases used by IJC or the information may become
            inconsistent.
        </p>

        <a name="dbschemas"></a>
        <p><strong>4. Database schemas</strong></p>
        <p>On Oracle you can make additional database schemas accessible to IJC. See the
            <a href="multi_schema.html">Support for multiple database schemas</a> page
            for more details.</p>

        <a name="miscellaneous"></a>
        <p><strong>5. Miscellaneous</strong></p>
        <p>Other schema related settings:</p>
        <ul>
            <li><strong>Turn off initial data retrieval.</strong> Avoids generating an initial hit
            list when you first use a view. This can be helpful for large tables where the 
            generation of the complete hist list can take some time. You must first run a query
            before you can see any data.</li>

            <li><strong>Enable event logging.</strong> Allows important events like changes to the
                database structure, or queries being executed to be written the log tables in the database.
            </li>

            <li><strong>Loading of database information.</strong> Provides different ways for loading the database
               information that IJC needs on startup. <br>
                There are 3 options for how the database information is retrieved on
                startup.<br>
                1. Load on start. Information for all tables that IJC is currently using is retrieved when IJC starts.<br>
                2. Load as needed. Nothing is loaded until it is needed.<br>
                3. Caching. Information for tables is cached along with the IJC schema information (in the
                IJC_SCHEMA database table) and used next time instead of being loaded from the database
                meta data or data dictionary tables. This should only be used when your database is not
                changing as the cached information would not be updated. To avoid this, when you connect
                to the IJC schema in "single user mode" you are automatically taken out of caching mode.<br>
                Whether Load on start or Load as needed is faster will depend on lots of factors, and you should
                experiment to see which is faster for you. Cached should be faster, but you should again check that
                this is indeed the case, and you should only use cached when you are not wanting to make
                changes to the tables or views in the database (editing data is fine, but creating or dropping
                tables, views or columns etc. is not).
            </li>
            
             <li><strong>Built it Schema login/logoff script.</strong> It is possible to execute groovy script syntax on
                 commencement of login to a schema and/or when logging out of a schema. 
                 The first built in closure (execution block) on_connect is synchronous with the schema initialisation process. 
                 Any code added to this closure will become part of that sequence of events. Some default implementation
                 is given (which can be uncommented and works best with a security policy in place!) which will display a dialog 
                 with the username displayed. The second closure, on_disconnect is synchronous with the schema disconnect process and again a default implementation
                 of a simple dialog is provided. You might wish to provide your own implementation of code within these closures but
                 blocking the initialisation of the schema should be avoided. If possible start an asynchronous thread and always catch
                 exceptions else they may go astray. A further groovy example is given below the "default contents" screenshot.  
                 
                 <br>
                 <img src="../../screenShots/SchemaLoginOutScript.png">
                 
             </li>
            
        </ul>
        
        <p> In this example we add the additional code to the default in order to write some data to a log table.
        
        </p>
        
        <pre>

        import groovy.sql.Sql

        on_connect = 
                 { schema, user ->
                 Thread.start{ logger(schema, user) }
                 }

                 logger = {  schema, user ->
                 try
                 {    
                 Sql sql = new Sql(schema.databasePlatform.dataSource);
                 (1..100).each 
                 {sql.eachRow('select * from log') 
                 {row -> println "${it}"}
                  sql.execute("insert into log (messages) values (${it})");  
                 }
                 sql.close();     
                 }
                 catch (exception e)
                 {println e.getMessage();}
                 }

        </pre>
        
        <a name="entities"></a><h3>Manage Entities</h3>
        <p>Entities can be added and removed from IJC using the Entities tab of the Schema Editor.</p>
        <ul>
            <li>Entities can be created, along with their corresponding database table(s)</li>
            <li>Entities can be promoted from a database table (or view) that is not currently used
                by any other entity
            </li>
            <li>Entities can be deleted. You are asked to confirm whether you want
                the database table to be deleted from the database.
            </li>
        </ul>

        <p>
            See the <a href="editing_entities.html">Editing Entities</a>
            page for more details about creating Entities.
        </p>

        <a name="fields"></a><h3>Manage Fields</h3>

        <p>Fields can be added and removed from IJC using the Schema Editor.</p>
        <ul>
            <li>Fields can be created, along with their corresponding database column</li>
            <li>Fields can be promoted from a database column that is not yet used by a
                Field.
            </li>
            <li>Fields can be deleted. You are asked to confirm whether you want
                the database column to be deleted from the database.
            </li>
        </ul>

        <p>
            See the <a href="editing_fields.html">Editing Fields</a>
            page for more details about creating Field.
        </p>

        <p>To delete an index in the Schema Editor:</p>
        <ol>
            <li>Find the index under the Indexes node of a table in the 'Tables' tab.</li>
            <li>Right click on the index and choose 'Delete...'.</li>
        </ol>
        <p>Note: not all indexes can be delete as some are required by the tables or
            foreign keys in the database.
        </p>

        <a name="relationships"></a><h3>Manage Relationships</h3>

        <p>Relationships define associations between entities.</p>

        <p>Relationships can be added and removed from IJC using the Schema Editor.</p>
        <ul>
            <li>Relationships can be created, with or without a corresponding foreign key
                constraint in the database.</li>
            <li>Relationships can be promoted from a foreign key
                constraint that is not currently used by the entity the field belongs to.
            </li>
            <li>Relationships can be deleted. You are asked to confirm whether you want
                the  foreign key to be deleted from the database.
            </li>
        </ul>
        <p>
            See the <a href="editing_relationships.html">Editing Relationships</a>
            page for more details about creating Relationships.
        </p>

         <a name="tables"></a><h3>Manage Tables & Views</h3>

        <p>Tables & Views provide the source objects for IJC entities.</p>

        <p>Tables are added to your database automatically when a new datatree or entity is created.
           It is best practice for the users of IJC to manage tables and views via managing the entities.
           It is also possible to integrate existing tables or views in a database schema but after new
           additions (for example using SQLPLUS) you will need to disconnect / reconnect from IJC in order to then view them in IJC.</p>
        <ul>
            <li>Tables can now be dropped from the database schema directly from IJC. This is really only required for orphaned tables
                i.e. tables that have no equivalent entity in the schema (or indeed a requirement for it). You cannot drop a table or view used by an entity.
            </li>
            <li>Tables and Views not associated with an entity, but are required can be promoted to entities if they do not exist and their columns promoted to fields
                for existing entities.
            </li>
            <li>You can now create indexes on tables directly within IJC.
            </li>
        </ul>
        <p> Promotion and use of existing database tables is described here:<a href="existing_schemas.html"> Using existing database tables </a></p>
        
        <a name="indexes"></a><h3>Manage Indexes</h3>

        <p>Indexes provide the database with a much faster way to find values in a database
            column. Situations where adding an index will be beneficial (or not) are listed
            in the <a href="../tips_and_tricks/performance_tips.html#indexes">Performance tips</a>
            tips and tricks page.</p>
        <p>Please consult your database administrator if you are not clear about indexes.</p>
        <p>To add an index in the Schema Editor:</p>
        <ol>
            <li>An index can be added using any of these approaches:
                <ul>
                    <li>Switch to the tables tab of the Schema Editor and find the table to
                        which you want the index adding. Select its 'Indexes' child node and choose
                        the 'New Index...' item from its right click popup menu</li>
                    <li>Locate a field whose column you want an index adding to in the
                        'Entities' or 'Data Trees' tab and choose
                        the 'New Index...' item from its right click popup menu.</li>
                    <li>Select an Entity or Field in the
                        'Entities' tab and click on the 'Add index' icon
                        (&nbsp;<img src="../../icons/index-new.png">&nbsp;)
                        in the Schema Editor toolbar.</li>
                </ul>
                The New Index dialog will open.
                <br>
                <br>
                <img src="../../screenShots/add-index.png">
                <br>
            </li>

            <li>Specify the column(s) that will be indexed. If you started the
                dialog with a Field selected then IJC will assume you want to add an
                index to the column used by that Field. If you had an Entity selected
                you need to specify the columns yourself. You add a column by selecting it
                from the 'Column' combo box and clicking on the 'Add' button. It will be added
                to the list displayed in the table.
            </li>
            <li>The currently specified column(s) can be changed by using the 'Up'. 'Down' and
                'Remove' buttons (note: if the index contains multiple columns the order is important).
                Usually you will only want a  single column in the index).
            </li>
            <li>The order the column values are stored in the database can be changed from
                ascending to descending using the checkbox for the column in the table.
                Usually the default will be OK.
            </li>
            <li>Specify a name for the index if the default provided is not satisfactory.</li>
            <li>If you want the index to restrict the values for those columns to be unique
                (duplicate values are not permitted) check the 'Unique keys' checkbox.
            </li>
            <li>Click on the 'Finish' button and the index will be added.
                Adding the index may take some time for large tables.</li>
        </ol>


                <br/>
                <hr/>
                <p style="font-size:xx-small;text-align:center">Copyright &#169; 1998-2013
                <a href='http://www.chemaxon.com' target='_top'>ChemAxon Ltd.</a>
                All rights reserved.</p>
                <br>

    </body>
</html>

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -v
bindkey '^R' history-incremental-search-backward
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/david/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
alias ls='ls -G'
alias ll='ls -l'
alias la='ls -a'
alias lm='ls -l --block-size=1M'
alias grep='grep --color=auto'
alias ..='cd ..'
alias jku='links https://login.jku.at'
alias hermes='ssh -X -l pechda00 hermes.prf.jcu.cz'
alias kiloseconds='bash bin/kiloseconds.sh'
#alias mc='mc -b'
alias vmup='VBoxHeadless -s Fedora\ 10_JChemCart_5.7 &'
alias vmdown='VBoxManage controlvm Fedora\ 10_JChemCart_5.7 poweroff &'
alias vmlist='VBoxManage list vms'
alias vmrlist='VBoxManage list runningvms'
alias jchemRepo='cd /Users/david/IJCrepo/IJC/'
alias safari='open -a Safari'
alias kouknout='qlmanage -p'
alias textedit='open -a TextEdit'
alias html2latex='sh /Users/david/.bin/html2latex/htmltolatex'
#mc alias solved Mac issue with .mc/ini colours not being recognised
alias mc='mc --colors=base_color=lightgray,green:normal=green,default:selected=white,gray:marked=yellow,default:markselect=yellow,gray:dir    ectory=blue,default:executable=brightgreen,default:link=cyan,default:device=brightmagenta,default:special=lightgray,    default:errors=green,default:reverse=green,default:gauge=green,default:input=white,gray:dnormal=green,gray:dfocus=br    ightgreen,gray:dhotnormal=cyan,gray:dhotfocus=brightcyan,gray:menu=green,default:menuhot=cyan,default:menusel=green,    gray:menuhotsel=cyan,default:helpnormal=cyan,default:editnormal=green,default:editbold=blue,default:editmarked=gray,    blue:stalelink=green,default'
alias TODO='vim /Users/david/GITrepos/synced/TODO'
alias mvim='open -a MacVim'
alias lojza='ssh dpech@lojza.flih.cz'
alias pdfmerge='/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py -o'

zmodload zsh/zftp
zmodload zsh/datetime

autoload -U promptinit
promptinit
prompt elite2 green
export EDITOR=vim

setopt autocd

## Correction
#setopt correctall

## Completion
zstyle :compinstall filename "$HOME/.zshrc"
zstyle ':completion:*:*:cd:*' tag-order local-directories path-directories
zstyle ':completion:*:rm:*' ignore-line yes                               
# color for completion                                                    
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}             
# menu for auto-completion                                                
zstyle ':completion:*' menu select=2                                      
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s

export PERL5LIB=$PERL5LIB:/usr/share/perl5/site_perl/5.10.1
export PATH=/opt/local/bin:/opt/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin:/usr/texbin:/usr/ebiotools/bin

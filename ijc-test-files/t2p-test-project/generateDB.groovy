import com.im.commons.db.util.*;
import groovy.xml.MarkupBuilder
import java.io.ByteArrayOutputStream;
import com.im.commons.util.RandomGUID
import com.im.df.impl.db.api.DBImplConstants
import com.im.df.api.support.DFNewTypeWellKnownOptions;

import static chemaxon.jchem.db.TableTypeConstants.*;
import static java.lang.Math.*

// ###########################################################################
// All you need to put this script into action within IJC is to acquire IJC
// DFSchema instance. For example in an IJC data tree script you can do this
// with "schema = dataTree.schema"
// ###########################################################################
// schema = <obtain DFSchema implementation here>

nEntities = 6

jChemEntityChance = 0.66

// Points in CDF, see code for details.

textFieldCumChance = 0.33     // Point on cumulative distribution function up to which text fields are created
floatFieldCumChance = 0.66    // Point on cumulative distribution function up to which float fields are created
intFieldCumChance = 1         // Point on cumulative distribution function up to which int fields are created

maxFields = 8                 // Maximum number of generated fields in a single entity

pkInRelationshipChance = 0.5  // How probable is it to create a relationship involving primary key

entityPrefix = "entity"
fieldPrefix = "field"
relationPrefix = "rel"
dtPrefix = "Data tree "

tfSize = 10                // size of text fields, in Unicode characters
tfLenMean = tfSize / 2     // Length of strings is normally distributed, this is the mean
tfLenStdDev = tfSize / 5   // and standard deviation of that distribution.

doubleMean = 0             // Doubles are normally distributed. You can specify the mean
doubleStdDev = 50          // and standard deviation of their distribution.

nRelations = nEntities * 3  // maximum number of relations between entities
nDataRows = nEntities * 100 // Total number of inserted rows

nDatatrees = nEntities * 2

nextNodeInDTChance = 0.7    // Chance to generate a child for the just generated vertex
dtBranchChance = 0.2        // Chance to generate a sibling for the just generated vertex (excluding the root vertex)

structurePoolName = "Pubchem demo" // Name of the entity from which the molecules are randomly
                              // chosen for insertion in the generated entities.
                              // We don't generate real random molecules. Sorry.

//seed = System.nanoTime()
seed = 1470458689031
rng = new Random(seed)


// ###########################################################################
//
// You should not need to edit anything below. But please experiment!
//
// ###########################################################################

println "Creating schema with random seed $seed"

// lookups
relNT = schema.relationships.newTypes.find { it.options instanceof DFNewTypeWellKnownOptions.NewRelationshipSimple }
dbSchemaName = DIFUtilities.getDefaultSchemaName(schema)
structurePool = schema.entities.items.find { it.name == structurePoolName }
if (!structurePool || !DIFUtilities.findCapability(structurePool, JChemEntityCapability.class)) {
    throw new Exception("JChem entity $structurePoolName not found. Please set structurePoolName to a JChem entity in this schema.")
}
structurePoolEdp = DFEntityDataProviders.find(structurePool)
structurePoolIdsList = structurePoolEdp.queryForIds(DFTermExpression.ALL_DATA, SortDirective.EMPTY, env)
structurePoolSField = DIFUtilities.findCapability(structurePool, JChemEntityCapability.class).structureField

// +---------------+
// |The entry point|
// +---------------+

createdEntities = createEntities(schema, rng)
println createdEntities
addData(createdEntities)
createDatatrees(createdEntities, schema, rng)

// +--------------------+
// |Function definitions|
// +--------------------+

/**
 * Creates entities and their data based on parameters in the script header.
 */
def createEntities(schema, rng) {
    def entities, entitiesFields, relations, relationsFromField, uniqueFields

    withLocked(schema.lockable) { env ->
        entities = (1..nEntities).collectEntries {
            entityName = entityPrefix + it
            if (rng.nextDouble() < jChemEntityChance) {
                entity = DFEntities.createJChemEntity(schema, entityName, TABLE_TYPE_MOLECULES, env)
            } else {
                entity = DFEntities.createStandardEntity(schema, entityName, env)
            }
            newDT = DFDataTrees.createDataTree(entity, entity.name, env)
            DIFUtilities.createViewForDataTree(newDT, null, true, env)

            println "Created $entity.name ... $entity"
            return [entity.name, entity]
        }

        entitiesFields = entities.collectEntries { eName, entity ->
            fields = (1..rng.nextInt(maxFields + 1)).collect {
                r = rng.nextDouble()
                if (r < textFieldCumChance) {
                    field = DFFields.createTextField(entity, fieldPrefix + " str " + it, fieldPrefix + it, tfSize, env)
                } else if (r < floatFieldCumChance) {
                    field = DFFields.createFloatField(entity, fieldPrefix + " float " + it, fieldPrefix + it, -1, -1, env)
                } else {
                    field = DFFields.createIntegerField(entity, fieldPrefix + " int " + it, fieldPrefix + it, env)
                }
                println "Created $eName -> $field.name ... $field"
                return field
            }
            return [entity, fields]
        }

        relationsFromField = [:]
        uniqueFields = [] as Set

        relations = (1..nRelations).collect {
            eNames = entities.keySet() as ArrayList
            Collections.shuffle(eNames, rng)

            e1 = entities[eNames.pop()]
            e1Fields = entitiesFields[e1]

            e2 = entities[eNames.pop()]
            e2Fields = entitiesFields[e2]

            if (e1Fields.empty) {
                if (e2Fields.empty) {
                    return null
                }
                destField = e1.idField
            } else if (rng.nextDouble() < pkInRelationshipChance) {
                destField = e1.idField
            } else {
                destField = e1Fields[rng.nextInt(e1Fields.size())]
            }

            e2CompatibleFields = e2Fields.findAll {
                destField.fieldClass.equals(it.fieldClass) && !Float.class.isAssignableFrom(it.fieldClass)
            }

            if (e2CompatibleFields.empty) {
                return null
            }

            srcField = e2CompatibleFields[rng.nextInt(e2CompatibleFields.size())]

            nto = relNT.options
            nto.createConstraints = true
            nto.srcField = srcField
            nto.dstField = destField
            nto.relType = DFRelationship.Type.MANY_TO_ONE
            nto.foreignKey.nameSafe = new SchemaQualifiedName(dbSchemaName, relationPrefix + it)


            if (relationsFromField.containsKey(srcField)
                || relationsFromField.containsKey(destField)
                || srcField in uniqueFields) {
                return null
            } else {
                println "Creating relation $it: ${e1.name}.$destField.name <- ${e2.name}.$srcField.name"
                relation = relNT.create(env).iterator().next()
                relationsFromField[srcField] = relation
                uniqueFields << destField
            }
            return relation
        }.findAll { it != null }
    }

    return new ArrayList(entities.values())
}

def addData(entities) {
    println ""
    println "Loading data"
    println ""

    def entitiesFields = entities.collectEntries {
        [it, it.fields.items.findAll { it.name.startsWith(fieldPrefix) }] // only fields created by us -- don't care about JChem managed fields
    }

    def fieldsValues = entitiesFields.collect { e, fields ->  [e.idField, fields] }.flatten()
                        .collectEntries { field -> [field, new RandomSet()] }

    println fieldsValues.collect { f, vals -> f.name }

    def relationsFromField = schema.relationships.items.collectEntries {
        [it.forward.srcField, it]
    }

    def uniqueFields = schema.relationships.items.collect { it.forward.destField }

    nDataRows.times {
        randomEntityIndex = rng.nextInt(entities.size())
        def entity = entities[randomEntityIndex]

        def row = [:]

        def jCEntCap = DIFUtilities.findCapability(entity, JChemEntityCapability.class)

        if (jCEntCap != null) {
            row[jCEntCap.structureField.id] = randomStructure()
        }

        entitiesFields[entity].each { field ->
            def value
            if (field in relationsFromField.keySet()) {
                def uniqueField = relationsFromField[field].forward.destField
                value = fieldsValues[uniqueField].chooseRandomly(rng)
            } else {
                value = randomValue(field.fieldClass, rng)
            }
            if (field in uniqueFields) {
                while (value == null || value == "" || value in fieldsValues[field]) {
                    value = randomValue(field.fieldClass, rng)
                }
            }
            if (value != null) {
                fieldsValues[field] << value
                row[field.id] = value
            }
        }

        def edp = DFEntityDataProviders.find(entity)
        withLocked(edp.lockable) { env ->
            vals = row.collectEntries { f, v -> [entity.fields.items.find { it.id == f }.name, v] }
            println "Inserting $it/$nDataRows (to ${entity.name})"
            updateInfo = edp.insert(row, null, env)
            fieldsValues[entity.idField] << updateInfo.id
        }
    }
}

def createDatatrees(entities, schema, rng) {
    withLocked (schema.lockable) { env ->
        def rels = schema.relationships.items
        nDatatrees.times {
            dTEntities = entities.clone()
            Collections.shuffle(dTEntities, rng)

            def newDT = DFDataTrees.createDataTree(dTEntities.pop(), dtPrefix + it, env)
            createBranch(dTEntities, newDT.rootVertex, rels, env, rng)

            DIFUtilities.createViewForDataTree(newDT, null, true, env)

            // No other way to find NT for a form view?
            viewNT = newDT.views.newTypes.find {( // bracket for groovy compiler to not end expression on line end
                (it.options in DFNewTypeWellKnownOptions.NewView)
                && (it.type == DBImplConstants.FORM_VIEW_STANDARD)
            )}

            def viewConfig = generateViewXml(newDT)

            withLocked(schema.userLockable) { userEnv ->
                def newView = viewNT.create(userEnv).iterator().next()
                newView.itemContent.setConfiguration(viewConfig, userEnv)
            }
        }
    }
}

def createBranch(entities, vertex, relations, env, rng) {
    while (rng.nextDouble() < nextNodeInDTChance) {
        if (rng.nextDouble() < dtBranchChance) {
            createBranch(entities, vertex, relations, env, rng)
        }
        def applicableRelDirs = relations.collect {
            if (it.forward.srcField.entity == vertex.entity) {
                return it.forward
            } else if (it.reverse.srcField.entity == vertex.entity) {
                return it.reverse
            }
            return null
        }.findAll { it != null && it.destField.entity in entities }
        relDir = randomChoice(applicableRelDirs, rng)
        if (relDir) {
            def edge = vertex.addEdge(relDir, env)
            entities.remove(edge.destination.entity)
            createBranch(entities, edge.destination, relations, env, rng)
        }
    }
}

def generateViewXml(dataTree) {
    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.scenesettings(borderSize:"5", internalBorderSpacingOn:"false", version:"2",
                          xmlns:"http://informaticsmatters.com/xml/ijc/1.0") {
        classifier() {
            binSets()
            expressionSets()
        }
        widgets() {
            def vertex = dataTree.rootVertex
            generateTablesForVertexAndChildren(vertex, 0, builder)
        }
    }
    return writer.toString()
}

def generateTablesForVertexAndChildren(vertex, int tablePos, builder) {
    generateTableXml(vertex.entity.fields.items, tablePos, builder)
    vertex.edges.eachWithIndex { edge, idx ->
        generateTablesForVertexAndChildren(edge.destination, 1 + tablePos + idx, builder)
    }
}

/**
 * @tablePos position of table in the form, starting from 0
 */
def generateTableXml(fields, int tablePos, MarkupBuilder xml) {
    def height = 287
    def width = 600
    def y = tablePos * height
    def colWidth = (width / fields.size()) as Integer

    def tableId = RandomGUID.create()
    xml.widget(borderSize: 5) {
        designerHandle(factoryId:"tableWidgetFactory", id:tableId) {
            boundFields() {
                fields.each { f ->
                    field(entityId: f.entity.id) {
                       mkp.yield f.id
                    }
                }
            }
            settings() {
                wbtablesettings() {
                    border() {
                       widgetBorder(borderType:"TITLE_WITH_SORT", font:"Dialog plain 11")
                    }
                    columns() {
                        fields.each { f ->
                            wbtablecolumnsettings(columnWidth:colWidth,
                                                    fieldId:f.id,
                                                    headerAlignment:"-1",
                                                    visible:"true") {
                               // Not needed, the default rendered is chosen
                               // createFieldRendererXml(f, xml)
                            }
                        }
                    }
                }
            }
        }
        position(height:height, width:width, y:y)
    }
}

def createFieldRendererXml(field, builder) {
    if (DIFUtilities.findCapability(field, DFFieldStructureCapability.class)) {
        builder.structureRenderer()
    } else if (Float.class.isAssignableFrom(field.fieldClass)) {
        builder.floatRenderer()
    } else if (Integer.class.isAssignableFrom(field.fieldClass)) {
        builder.integerRenderer()
    } else {
        builder.multiLineTextRenderer()
    }
}

public static randomChoice(list, rng) {
    if (list.empty) {
        return null
    }
    return list[rng.nextInt(list.size())]
}

class RandomSet {
    def map = [:]
    def list = []

    def leftShift(o) {
        if (o in map.keySet()) {
            return
        } else {
            map[o] = list.size()
            list << o
        }
    }

    def isCase(o) {
        return o in map.keySet()
    }

    def chooseRandomly(Random rng) {
        // No way to call randomChoice from here unless resorting to create closure and put it into binding?
        if (list.empty) {
            return null
        }
        return list[rng.nextInt(list.size())]
    }
}

public <T> T randomValue(Class<T> cls, Random rng) {
    if (String.class.isAssignableFrom(cls)) {
        return randomUTFString(rng)
    } else if (Float.class.isAssignableFrom(cls)) {
        return (rng.nextGaussian() * doubleStdDev) + doubleMean as Float
    } else if (Integer.class.isAssignableFrom(cls)) {
        return rng.nextInt()
    } else {
        throw new IllegalArgumentException(cls as String)
    }
}

def randomStructure() {
     def id = randomChoice(structurePoolIdsList, rng)
     def data = structurePoolEdp.getData([id], env) // [structurePoolSField], env) see IJC-5036 Error when querying subset of entity fields

     return data[id][structurePoolSField.id]
}

public String randomUTFString(Random rng) {
    def os = new ByteArrayOutputStream(tfSize)

    tfSize.times {
        os.write(rng.next(8))
    }

    wantedLength = ((rng.nextGaussian() * tfLenStdDev) + tfLenMean) as Integer

    str = os.toString("UTF-8") // invalid values are replaced by some default replacement character
    length = max(0, min(wantedLength, str.length()))

    return str.substring(0, length).trim() // unique key in Derby seemed to not distinguish "a " and "a"
}

/**
 * Run closure with locked object. This method doesn't wait for the lock. If it is not available, an exception is
 * thrown.
 *
 * @param <T> closure's return type
 * @param some lockable
 * @param closure closure to call
 * @return the result of closure
 * @throws Exception if the closure throws some
 */
public <T> T withLocked(DFLockable some, String message=null, Closure<T> closure) throws Exception {
    DFLock lock = some.obtainLock(message);
    DFEnvironmentRW envRW = null;
    def finish = true;
    try {
        if (message==null) {
            envRW = EnvUtils.createRWFromRO(env, lock);
            finish = false
        } else {
            envRW = EnvUtils.createDefaultEnvironmentRW(lock, message, true)
        }
        return closure.call(envRW);
    } finally {
        lock.release(); // should never be null
        if (finish && envRW != null) {
            envRW.getFeedback().finish();
        }
    }
}



// change the usr/pwd pairs to match accounts in your 'T2P Source' and 'T2P Target' machine databases
def pwds = ['t2p', 't2p', 't2p', 't2p']

def cmds = ['java', '-jar',  projectFile('test2production.jar').absolutePath, '--apply', projectFile('.config' + File.separator + schema.name + '.ijs').absolutePath, projectFile('.config' + File.separator + schema.name + '_1.ijs').absolutePath]


if(binding.variables.containsKey('embedded') && binding.getVariable('embedded')){
    runEmbedded(cmds, pwds)
} else {
    runAsProcess(cmds, pwds)
}

def runAsProcess(cmds, pwds){
    def proc = Runtime.runtime.exec(cmds as String[])
    proc.outputStream.withWriter{ writer ->
       pwds.each { writer.println it }
    }
    proc.waitFor()
    
    [out: cleanString(proc.inputStream.text), err: cleanString(proc.errorStream.text)]
}

def runEmbedded(cmds, pwds){
    ByteArrayOutputStream outos = new ByteArrayOutputStream()
    PrintStream out = new PrintStream(outos)
    ByteArrayOutputStream erros = new ByteArrayOutputStream()
    PrintStream err = new PrintStream(erros)
    String data = pwds.join('\n') + '\n'
    InputStream input = new ByteArrayInputStream( data.getBytes("UTF-8") )
    PrintStream oldOut = System.out
    PrintStream oldErr = System.err
    InputStream oldIn  = System.in
    try {
        System.out = out
        System.err = err
        System.in  = input
        com.chemaxon.t2p.T2PRunner.main(cmds[3..-1] as String[])         
    } finally {
        System.out = oldOut
        System.err = oldErr
        System.in  = oldIn
    }
    
    [out: cleanString(outos.toString("UTF-8")), err: cleanString(erros.toString("UTF-8"))]
}


def projectFile(String relativePath) {
    new File(projectDirectoryPath, relativePath)
}

def getProjectDirectoryPath(){
    if(binding.variables.containsKey('projectDirectory')){
        return binding.getVariable('projectDirectory').absolutePath
    }
//    def project = com.im.ijc.core.api.util.ProjectUtils.'default'?.selectedProject
    def project = org.netbeans.api.project.ui.OpenProjects.getDefault().getOpenProjects()[0]
    if(!project){
        throw new IllegalStateException("Project directory is not set yet. Please, open any view or form and run the script again")
    }
    project.projectDirectory.path
}

String cleanString(String messy){
    messy.trim().replaceAll(/\s+$/,'').replaceAll(/^\s+/, '').replaceAll(/\s+/, ' ')
}

// change this to 'true' for running tests in 'T2P Target' machine
target = false

// testing against derby or other databases
embedded = false

// set to true if testing legacy 5.11.X version of IJC
legacy = false


// ---------------------------------------------
// -------- NO EDITS BELOW THIS LINE --------
// ---------------------------------------------

// dsl support
dsl = include 'dsl.groovy'

// sync latest changes so we start with databases synchronized
include 'runner.groovy'

(1..(target ? 5 : 9)).each {
    runStep it
}

def runStep(int number){
    def base = "steps/${target ? 'target':'source'}/${number.toString().padLeft(2,'0')}"
    try {
        println "Running ${target ? 'target' : 'source'} step $number"
        include "${base}.setup"
        if(assertExec(number)){
            println "${target ? 'Target' : 'Source'} step $number OK"
        } else {
            err << "${target ? 'Target' : 'Source'} step $number FAILED" << '\n'
        }
    } catch(e) {
        err << "\n${e.getClass().simpleName} including ${target ? 'target' : 'source'} step $number:" << '\n'
        err << e.message << '\n'
    } finally {
        try {
           include "${base}.cleanup"
        } finally {
           // sync whatever the state is to start next step with synced dbs
           include 'runner.groovy'
        }
    }
}

def assertExec(int stepNumber){
    try {
        def output = include('runner.groovy').out - 'source database username: source database password: destination database username: destination database password: '
        println "Output: $output"
        def expected = readTemplate("steps/${target ? 'target':'source'}/${stepNumber.toString().padLeft(2,'0')}.output")
        if(!expected){
            expected = 'Destination schema is already up to date'
        }
        def splitter = /\s+\*\s+/
        output   = new LinkedHashSet(output.split(splitter).toList())
        expected = new LinkedHashSet(expected.split(splitter).toList())
        assert output == expected
        return true
    } catch(Throwable e) {
        err << "\n${e.getClass().simpleName} asserting tool output for ${target ? 'target' : 'source'} Step $stepNumber:"
        err << e.message << '\n'
        return false
    } 
}

def include(String relativePath){
    File script = new File(projectDirectoryPath, relativePath)
    if(script.exists()){
        new GroovyShell(getClass().classLoader, binding).evaluate(script)
    }
}

def readTemplate(String relativePath){
    def engine = new groovy.text.SimpleTemplateEngine(getClass().classLoader)
    def tpl = new File(projectDirectoryPath, relativePath)
    if(!tpl.exists()){
        return ''
    }
    engine.createTemplate(tpl.text).make(new HashMap(binding.variables)).toString().trim().replaceAll(/\s+$/,'').replaceAll(/^\s+/, '').replaceAll(/\s+/, ' ')
}

def getProjectDirectoryPath(){
    if(binding.variables.containsKey('projectDirectory')){
        return binding.getVariable('projectDirectory').absolutePath
    }
//    def project = com.im.ijc.core.api.util.ProjectUtils.'default'?.selectedProject
    def project = org.netbeans.api.project.ui.OpenProjects.getDefault().getOpenProjects()[0]
    if(!project){
        throw new IllegalStateException("Project directory is not set yet. Please, open any view or form and run the script again")
    }
    project.projectDirectory.path
}
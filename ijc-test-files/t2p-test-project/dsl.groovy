class DSL {

    static enum EntityType { JCHEM, STANDARD }
    
    def schema
    def env
    def binding

    DSL(schema, env, binding){
        this.schema = schema
        this.env    = env
        this.binding=binding
    }
    
    Class getEntity()        { com.im.df.api.ddl.DFEntity }
    Class getField()         { com.im.df.api.ddl.DFField }
    Class getDatatree()      { com.im.df.api.ddl.DFDataTree }
    Class getRelationship()  { com.im.df.api.ddl.DFRelationship }
    Class getScript()        { com.im.df.api.ddl.DFScript }        
    Class getView()          { com.im.df.api.ddl.DFView }
    Class getQuery()         { com.im.df.api.ddl.DFQuery }
    EntityType getJchem()    { EntityType.JCHEM }
    EntityType getStandard() { EntityType.STANDARD }
    
    ContainerFinder get(Class cls){
        switch(cls){
            case com.im.df.api.ddl.DFEntity:         return new ContainerFinder(schema.entities, env)
            case com.im.df.api.ddl.DFDataTree:       return new ContainerFinder(schema.dataTrees, env)
            case com.im.df.api.ddl.DFRelationship:   return new ContainerFinder(schema.relationships, env)
            case com.im.df.api.ddl.DFScript:         return new ContainerFinder(schema.scripts, env)
            default:                                 throw new IllegalArgumentException("Getting items of type $cls not supported for schema")            
        }
    }
    
    DeleteCommand delete(Class cls){
        switch(cls){
            case com.im.df.api.ddl.DFEntity:         return new DeleteCommand(schema.entities, env)
            case com.im.df.api.ddl.DFDataTree:       return new DeleteCommand(schema.dataTrees, env)
            case com.im.df.api.ddl.DFRelationship:   return new DeleteCommand(schema.relationships, env)
            case com.im.df.api.ddl.DFScript:         return new DeleteCommand(schema.scripts, env)
            default:                                 throw new IllegalArgumentException("Deleting items of type $cls not supported for schema")            
        }
    }
    
    def from(com.im.df.api.ddl.DFItem item) {
        new ContainerAccessor(item, env)
    }
    
    def get(String name){
        binding[name]
    }
    
    def set(String name, item){
        binding[name] = item
    }
    
    def rename(item){
        new RenameCommand(item, env)
    }
    
    def create(EntityType type){
        new CreateEntityCommand(schema, EntityType.JCHEM == type, env)
    }
    
    def create(Class cls){
        switch(cls){
            case com.im.df.api.ddl.DFDataTree:       return new CreateDatatreeCommand(this, env)
            case com.im.df.api.ddl.DFView:           return new CreateViewCommand(this, env)
            case com.im.df.api.ddl.DFEntity:         throw new IllegalArgumentException("Please specify type of entity either 'create jchem entity' or 'create standard entity'")            
            default:                                 throw new IllegalArgumentException("Creating items of $cls not supported for schema")            
        }
    }
    
    def call(Closure closure){
        with closure
    }
}

@groovy.transform.Canonical
class ContainerFinder {
    def container
    def env
    
    com.im.df.api.ddl.DFItem get(String name){
        if(name ==~ /\d+/){
            return get(Integer.valueOf(name))
        }
        def result = container.items.find{ it.name == name }
        if(!result){
            throw new IllegalArgumentException("Item with name $name not found in $container")
        }
        result
    }
    
    com.im.df.api.ddl.DFItem get(Integer index){
        if(index >= container.items.size() || index < 0){
             throw new IllegalArgumentException("There is no item at index $index  in $container")
        }
        container.items[index]
    }    
}

@groovy.transform.Canonical
class ContainerAccessor {
    def item
    def env
    
    ContainerFinder get(Class cls){
        switch(cls){
            case com.im.df.api.ddl.DFView:           return new ContainerFinder(item.views, env)
            case com.im.df.api.ddl.DFQuery:          return new ContainerFinder(item.queries, env)
            case com.im.df.api.ddl.DFField:          return new ContainerFinder(item.fields, env)
            case com.im.df.api.ddl.DFScript:         return new ContainerFinder(item.scripts, env)
            default:                                 throw new IllegalArgumentException("Getting items of $cls not supported for ${item.getClass()}")            
        }
    }
    
    DeleteCommand delete(Class cls){
        switch(cls){
            case com.im.df.api.ddl.DFView:           return new DeleteCommand(item.views, env)
            case com.im.df.api.ddl.DFQuery:          return new DeleteCommand(item.queries, env)
            case com.im.df.api.ddl.DFField:          return new DeleteCommand(item.fields, env)
            case com.im.df.api.ddl.DFScript:         return new DeleteCommand(item.scripts, env)
            default:                                 throw new IllegalArgumentException("Deleting items of $cls not supported for ${item.getClass()}")            
        }
    }
}

@groovy.transform.Canonical
class DeleteCommand {
    def container
    def env
    
    void get(String name){
        if(name ==~ /\d+/){
            get(Integer.valueOf(name))
            return
        }
        def item = container.items.find{ it.name == name}
        if(!item) { throw new IllegalArgumentException("No such item $name in $container") }
        Lockables.withLocked(item, "Deleting $name from $container", env) { envRW ->
            container.remove(item, false, envRW)   
        }
    }
    
    com.im.df.api.ddl.DFItem get(Integer index){
        if(index >= container.items.size() || index < 0){
             throw new IllegalArgumentException("There is no item at index $index  in $container")
        }
        get(container.items[index].name)
    }
}

@groovy.transform.Canonical
class RenameCommand {
    def item
    def env
    
    def to(String newName){
        Lockables.withLocked(item, "Rename $item.name to $newName", env) { envRW ->
            item.setName(newName, envRW)   
        }
        item
    }
}

@groovy.transform.Canonical
class CreateEntityCommand {
    def schema
    boolean jchem
    def env
    
    com.im.df.api.ddl.DFEntity entity(String name){
        Lockables.withLocked(schema, "Create entity $name", env) { envRW ->
            if(jchem){
                return com.im.df.api.ddl.DFEntities.createJChemEntity(schema, name, envRW)
            }
            return com.im.df.api.ddl.DFEntities.createStandardEntity(schema, name, envRW)
        }
    }
}


@groovy.transform.Canonical
class CreateDatatreeCommand {
    def dsl
    def env
    String name
      
    com.im.df.api.ddl.DFDataTree from(com.im.df.api.ddl.DFEntity entity){
        Lockables.withLocked(entity, "Create datatree $name", env) { envRW ->
            com.im.df.api.ddl.DFDataTrees.createDataTree(entity, name ?: entity.name, envRW)
        }
    }
    
    com.im.df.api.ddl.DFDataTree from(String entityName){
        from(dsl.with{ get entity entityName })
    }
    
    CreateDatatreeCommand called(String name){
        this.name = name
        this
    }
}

@groovy.transform.Canonical
class CreateViewCommand {
    def dsl
    def env
    String name
      
    com.im.df.api.ddl.DFView on(com.im.df.api.ddl.DFDataTree datatree){
        Lockables.withLocked(datatree, "Create view $name", env) { envRW ->
            com.im.df.api.util.DIFUtilities.createViewForDataTree(datatree, name ? name : null, true, envRW)
        }
    }
    
    com.im.df.api.ddl.DFView on(String dtName){
        from(dsl.with{ get datatree dtName })
    }
    
    CreateDatatreeCommand called(String name){
        this.name = name
        this
    }
}

class Lockables {
    static <T> T withLocked(item, String message = null, env, Closure<T> closure) throws Exception {
        com.im.commons.progress.DFLockable some = com.im.df.api.util.DIFUtilities.getLockable(item)
        com.im.commons.progress.DFLock lock = some.obtainLock(message);
        com.im.commons.progress.DFEnvironmentRW envRW = null;
        def finish = true;
        try {
            if (message==null) {
                envRW = com.im.commons.progress.EnvUtils.createRWFromRO(env, lock);
                finish = false
            } else {
                envRW = com.im.commons.progress.EnvUtils.createDefaultEnvironmentRW(lock, message, true)
            }
            return closure.call(envRW);
        } finally {
            lock.release(); // should never be null
            if (finish && envRW != null) {
                envRW.getFeedback().finish();
            }
        }
    }
}

// inform and return dsl instance
println "DSL support loading successfull"
new DSL(schema, env, binding)
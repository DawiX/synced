#!/usr/bin/perl
use strict;
use warnings;
use File::Basename;
use File::Copy;

#Prints the list of icons to be copied to STDIN

my @newlist;

print "Define the file to extract lines from \n";
my $from = <STDIN>;
print "Define the file containing what to extract \n";
my $what = <STDIN>;
#print "Define the trunk root directory\n";
#my $root = <STDIN>;
#chomp($root);
#print "$root";

open INPUTFILE, $from; 
my @inputfile = <INPUTFILE>;
close INPUTFILE;

open WHATFILE, $what;
my @whatfile = <WHATFILE>;
close WHATFILE;

foreach my $line (@inputfile) {
	foreach my $word (@whatfile) {
		if ($line =~ m/$word/gi) {
		#open WHEREFILE, '>>$where';
		#print WHEREFILE "$line";
		#close WHEREFILE;
		print "$line";
	}
}
}

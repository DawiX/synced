#!/usr/bin/perl
use strict;
use warnings;
use File::Basename;
use File::Copy;


print "Define the file containing list of htmlFiles \n";
my $list = <STDIN>;

print "Where is the htmldocs folder?\n";
my $root = <STDIN>;
chomp($root);

print "Where is the css file?\n";
my $css = <STDIN>;
chomp($css);

open INPUTFILE, $list; 
my @inputfile = <INPUTFILE>;
close INPUTFILE;


foreach my $line (@inputfile) {
	chomp($line);
	my $root2 = $root;
	my $source = $root . $line;
	my $basename  = basename($line);
	my $newName = $basename;
	$newName =~ s/html/pdf/g;
	my $outPATH = $line;
	$outPATH =~ s/$basename/$newName/g;
	my $output = $root . $outPATH;
	my @args = ("wkpdf", "-s", $source, "-o", $output, "-y", $css);
	system(@args);
#	print "$css";
}

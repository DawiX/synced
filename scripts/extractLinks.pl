#!/usr/bin/perl
#
#Creates a list of all PDFs from the ijcTOC.html file,
#so that when the single PDFs are contatenated, they 
#are in the correct order according to TOC
#
#For now the list should be parsed to a file and 
#modified with VIM (to get rid of a href )
#
$filename = $ARGV[0];
require HTML::LinkExtor;
$p = HTML::LinkExtor->new(\&cb, $filename);
sub cb {
	my($tag, %links) = @_;
	print "$tag @{[%links]}\n";
}
$p->parse_file($filename); 

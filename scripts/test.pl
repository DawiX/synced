#!/usr/bin/perl

my $filename = $ARGV[0];
my $output = $ARGV[1];
require PDF::FromHTML;

my $pdf = PDF::FromHTML->new( encoding => 'utf-8' );
#load from file
$pdf->load_file(\$filename);

#perform conversion
$pdf->convert(
	Font 	=> 'Arial.tff',
	LineHeight => 10,
	Landscape => 0,
);

#write to file:
$pdf->write_file(\$output);
